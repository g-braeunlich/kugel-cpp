#ifndef __KGL_INPUT_HPP__
#define __KGL_INPUT_HPP__

namespace input {
  struct Event {
    enum Type {BUTTON, KEY, NONE};
    enum Key {SPACE, P, Q, R, ESC, UNKNOWN, UP, DOWN, LEFT, RIGHT};
    Key key;
    unsigned int button;
    Type type;
  };
}

#endif //__KGL_INPUT_HPP__
