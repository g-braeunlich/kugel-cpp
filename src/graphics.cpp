#include "graphics.hpp"

namespace gfx {
  double get_L(const Color &clr) {
    if(clr.r >= clr.g && clr.r >= clr.b)
      return clr.r;
    if(clr.g >= clr.b && clr.g >= clr.r)
      return clr.g;
    return clr.b;
  }
}
