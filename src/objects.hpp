#ifndef __OBJECTS_HPP__
#define __OBJECTS_HPP__

#include <memory>

#include "graphics.hpp"
#include "effects.hpp"

class Object {
public:
  virtual void accept(class Visitor&) = 0;
  virtual void accept_const(class ConstVisitor&) const = 0;
  virtual ~Object() = 0;
protected:
  Object() = default;
};

class Movable : public Object {
public:
  double x, y;      ///< Position
  double m_i, m_h;  ///< Inert / heavy mass
  double tau;       ///< Durability
  bool substantial;
  bool fixed;
  gfx::Color color;
  std::string label;

  double vx, vy;    ///< Velocity
  double ax, ay;    ///< Accelleration
  std::unique_ptr<DecayEffect> on_decay;
  std::unique_ptr<CollisionEffect> on_collision;
  void move(double dt);
  void accellerate(double dt);

  ~Movable();
  Movable();
};

class Ball : public Movable {
public:
  double r;	 ///< radius
  void accept(Visitor&);
  void accept_const(ConstVisitor&) const;
  Ball();
};

class Wall : public Object {
public:
  double nx, ny; ///< x- und y-components of normal
  double d; ///< Distance to origin along the normal
  void accept(Visitor&);
  void accept_const(ConstVisitor&) const;
  Wall(double x1, double y1, double x2, double y2);
  Wall();
};

class Rectangle : public Movable {
public:
  double w, h; ///< width, height
  void accept(Visitor&);
  void accept_const(ConstVisitor&) const;
  Rectangle();
};

class Visitor {
public:
  virtual void visit(Ball*) = 0;
  virtual void visit(Rectangle*) = 0;
  virtual void visit(Wall*) = 0;
  virtual ~Visitor() = 0;
};

class ConstVisitor {
public:
  virtual void visit(const Ball*) = 0;
  virtual void visit(const Rectangle*) = 0;
  virtual void visit(const Wall*) = 0;
  virtual ~ConstVisitor() = 0;
};

#endif // __OBJECTS_HPP__
