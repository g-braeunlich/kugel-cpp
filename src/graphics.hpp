#ifndef __GRAPHICS_HPP__
#define __GRAPHICS_HPP__

namespace gfx {

  struct Color {
    double r, g, b, a;
  };
  constexpr Color white = Color{1., 1., 1., 1.};
  constexpr Color black = Color{0., 0., 0., 1.};

  double get_L(const Color &clr);
  
  struct LineStyle {
    double width;
    Color color;
    double *dash_lengths; double dash_offset; int n_dashes;
  };

  constexpr LineStyle default_line_style = LineStyle{.width=1., .color = black, .dash_lengths = nullptr, .dash_offset = 0., .n_dashes = 0};

  enum FontSlant {
		  FONT_SLANT_NORMAL,
		  FONT_SLANT_ITALIC,
		  FONT_SLANT_OBLIQUE
  };

  enum FontWeight {
      FONT_WEIGHT_NORMAL,
      FONT_WEIGHT_BOLD
    };
  
  struct Font {
    FontSlant slant;
    FontWeight weight;
    char const* family;
    double size;
    Color color;
  };

}

#endif // __GRAPHICS_HPP__
