#ifndef __MULTI_DISPATCH_HPP__
#define __MULTI_DISPATCH_HPP__

#include <memory>
#include "objects.hpp"

template<typename O0, typename... O>
void visit_all(std::unique_ptr<Visitor> &V, O0 &o0, O&... objects) {
  visit_all(V, o0);
  visit_all(V, objects...);
}
template<typename O>
void visit_all(std::unique_ptr<Visitor> &V, O &o) {
  Visitor *v = V.get();
  o.accept(*v);
}


template <typename Ret, typename Arg, typename ...O>
class Dispatchable {
public:
  virtual Ret dispatch(Arg arg, O&...) = 0;
  virtual ~Dispatchable() = default;
};

template <typename dispatch_table, typename Ret, typename Arg, typename ...O>
struct ObjPrmPackNamespace {
  template <typename ...T>
  struct Dispatcher : public Dispatchable<Ret, Arg, O...> {
    Ret dispatch(Arg arg, O&... o)
    { return dispatch_table::dispatch(arg, dynamic_cast<T&>(o)...); }
  };
  
  typedef std::unique_ptr<Dispatchable<Ret, Arg, O...>> dispatcher_type;

  template<typename ...T>
  class FinalDispatchVisitor : public Visitor {
    dispatcher_type &dispatcher;
    template <class T_new>
    void visit_t(T_new*)
    { dispatcher.reset(new Dispatcher<T..., T_new>()); }
  public:
    void visit(Ball* obj) { visit_t(obj); }
    void visit(Rectangle* obj) { visit_t(obj); }
    void visit(Wall* obj) { visit_t(obj); }
  
    FinalDispatchVisitor(std::unique_ptr<Visitor>&, dispatcher_type &_dispatcher) : dispatcher(_dispatcher) {}
    ~FinalDispatchVisitor() = default;
  };

  template < template<typename... > class next_visitor, typename ...T>
  class DispatchVisitorSpecializer : public Visitor {
    std::unique_ptr<Visitor> &target_visitor;
    dispatcher_type &dispatcher;
    
    template <class T_new> void visit_t(T_new*)
    { target_visitor.reset(new next_visitor<T..., T_new>(target_visitor, dispatcher)); }
  public:
    void visit(Ball* obj) { visit_t(obj); }
    void visit(Rectangle* obj) { visit_t(obj); }
    void visit(Wall* obj) { visit_t(obj); }
    DispatchVisitorSpecializer(std::unique_ptr<Visitor> &_target_visitor, dispatcher_type &_dispatcher)
      : target_visitor(_target_visitor), dispatcher(_dispatcher) {}
    ~DispatchVisitorSpecializer() = default;
  };

  template <typename ...count> struct DispatchVisitorNamespace;
  template <typename count0, typename ...count>
  struct DispatchVisitorNamespace<count0, count...> {
    template <typename ...T>
    using DispatchVisitor = DispatchVisitorSpecializer<DispatchVisitorNamespace<count...>::template DispatchVisitor, T...>;
  };
  template <typename count0>
  struct DispatchVisitorNamespace<count0> {
    template <typename ...T>
    using DispatchVisitor = FinalDispatchVisitor<T...>;
  };
  using DispatchVisitor = typename DispatchVisitorNamespace<O...>::template DispatchVisitor<>;

  static inline Ret dispatch(Arg arg, O&... objects) {
    std::unique_ptr<Visitor> visitor;
    std::unique_ptr<Dispatchable<Ret, Arg, O...>> dispatcher;
    
    visitor.reset(new DispatchVisitor(visitor, dispatcher));
    visit_all(visitor, objects...);
    return dispatcher->dispatch(arg, objects...);
  }
};

template<typename dispatch_table, typename Ret, typename Arg, typename... O>
Ret dispatch(Arg arg, O&... objects)
{ return ObjPrmPackNamespace<dispatch_table, Ret, Arg, O...>::dispatch(arg, objects...); }

#endif // __MULTI_DISPATCH_HPP__
