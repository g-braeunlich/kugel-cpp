#include <cmath>
#include "objects.hpp"

void Movable::move(double dt) {
  x += vx*dt, y += vy*dt;
  if(tau > 0.) tau -= dt;
}
void Movable::accellerate(double dt) {
  vx += ax*dt, vy += ay*dt;
}

void Ball::accept(Visitor &v) { v.visit(this); }
void Wall::accept(Visitor &v) { v.visit(this); }
void Rectangle::accept(Visitor &v) { v.visit(this); }

void Ball::accept_const(ConstVisitor &v) const { v.visit(this); }
void Wall::accept_const(ConstVisitor &v) const { v.visit(this); }
void Rectangle::accept_const(ConstVisitor &v) const { v.visit(this); }

Ball::Ball() : r{1.} {}
Wall::Wall() : nx{1.}, ny{0.}, d{0.} {}
Wall::Wall(double x1, double y1, double x2, double y2) {
  static double dn;
  nx = -(y2-y1);
  ny = x2-x1;
  dn = hypot(nx, ny);
  if(dn == 0.) { nx = 1., ny = 0.; }
  else { nx /= dn, ny /= dn; }
  d = x1 * nx + y1 * ny;
}
Rectangle::Rectangle() {}
Movable::Movable() : x{0.}, y{0.}, m_i{0.}, m_h{0.}, tau{0.}, substantial{true}, fixed{false}, color{gfx::black}, ax{0.}, ay{0.} {}
Movable::~Movable() {}

Object::~Object() = default;
Visitor::~Visitor() = default;
ConstVisitor::~ConstVisitor() = default;
