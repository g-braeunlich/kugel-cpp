#include <cmath>
#include "numerics.hpp"

constexpr unsigned int N = 100;
constexpr double undefined = -2.;

double zero_p4(const double a[5], double eps) {
  /*
   *  Find smallest real zero x0 of
   *  p(x) = \sum_{n=0}^4 a[n] x^n
   *  such that
   *  x0 >= 0 and p'(x0) <= 0
   */
  static double q, q2, r, r2, temp, temp2, t, d1, d2, det, D;
  // Part 1: Factorize p(x) ~ (x^2 + q x + r)(x^2 + q2 x + r2)
  q = a[3] / a[4];
  r = a[2] / a[4];
  for(unsigned int _=0; _<N; _++) {
    d1 = a[1] + (q*q - r)*a[3] + q*(2*r - q*q)*a[4] - q*a[2];
    d2 = a[0] - r*a[2] - r*(q*q-r)*a[4] + r*q*a[3];
    if(fabs(d1) < eps && fabs(d2) < eps) break;
    det = r*(-a[3] + 2*q*a[4])*(-a[3] + 2*q*a[4])
      + (-a[2] + 2*q*a[3] + (2*r-3*q*q)*a[4])*(-a[2] + q*a[3] + (2*r-q*q)*a[4]);
    // Newton:
    // d(x0) + Dd(x0)(x-x0) = 0 => x = x0 - Dd^-1 d(x0)
    temp = ((-a[2] + (2*r-q*q)*a[4] + q*a[3])*d1 + (a[3] - 2*q*a[4])*d2) / det;
    r -= ((2*r*q*a[4] - r*a[3])*d1 + (-a[2] + 2*q*a[3] + (2*r-3*q*q)*a[4])*d2) / det;
    q -= temp;
  }
  // Part 2: Examine zeros
  D = q*q - 4.*r;
  // [-b -s*sqrt(b^2 - 4ac)]/(2a) = 2c/[-b +s*sqrt(b^2 - 4ac)]
  // p'(x_0) <= 0
  // p(x) = p_1(x) p_2(x)
  // p_1(x_0) = 0 => p'(x_0) = p_1'(x_0) p_2(x_0)
  t = undefined;
  q2 = a[3]/a[4]-q;
  r2 = a[2]/a[4] + (q*q-r) - q*a[3]/a[4];
  if(D >= 0.) {
    temp = -q - copysign(sqrt(D), q);
    temp2 = 2.*r / temp;
    temp *= 0.5;
    if(temp >= -eps && (2.*temp + q)*(temp*(temp + q2) + r2) <= 0.)
      t = temp;
    if(-eps <= temp2 && (temp2 < t || t < 0.) && (2.*temp2 + q)*(temp2*(temp2 + q2) + r2) <= 0.)
      t = temp2;
  }
  D = q2*q2 - 4.*r2;
  if(D >= 0.) {
    temp = -q2 - copysign(sqrt(D), q2);
    temp2 = 2.*r2 / temp;
    temp *= 0.5;
    if(-eps <= temp && (temp < t || t < 0.) && (2.*temp + q2)*(temp*(temp + q) + r) <= 0.)
      t = temp;
    if(-eps <= temp2 && (temp2 < t || t < 0.) && (2.*temp2 + q2)*(temp2*(temp2 + q) + r) <= 0.)
      t = temp2;
  }
  return t;
}
