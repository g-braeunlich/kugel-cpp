#define _USE_MATH_DEFINES
#include <cmath>
#include "canvas_cairo.hpp"

#include "graphics-render.hpp"

extern "C" {
#include <cairo/cairo.h>
}

CanvasCairo::CanvasCairo(cairo_surface_t *sfc, int w, int h) : w{w}, h{h}, sfc{sfc} {
  ctx = cairo_create(sfc);
}

CanvasCairo::~CanvasCairo() {
  cairo_destroy(ctx);
  cairo_surface_destroy(sfc);
}

void CanvasCairo::render(const Kernel &core) {
  cairo_push_group(ctx);
  render_cairo(ctx, core);
  cairo_pop_group_to_source(ctx);
  cairo_paint(ctx);
  cairo_surface_flush(sfc);
}


static void set_color(void *bmp, const gfx::Color &clr) {
  cairo_set_source_rgba((cairo_t*)bmp, clr.r, clr.g, clr.b, clr.a);
}
  
static void set_line_style(cairo_t *cr, const gfx::LineStyle &style) {
  set_color(cr, style.color);
  if(style.n_dashes != 0) cairo_set_dash(cr, style.dash_lengths, style.n_dashes, style.dash_offset);
  cairo_set_line_width(cr, style.width);
}
  
void draw_rectangle(cairo_t *cr, double x, double y, double w, double h, const gfx::LineStyle &line_style) {
  cairo_save(cr);
  set_line_style(cr, line_style);
  cairo_rectangle(cr, (double)x, (double)y, (double)w, (double)h);
  cairo_stroke(cr);
  cairo_restore(cr);
}
  
void draw_rectangle_filled_with_color(cairo_t *cr, double x, double y, double w, double h, const gfx::Color &c) {
  cairo_save(cr);
  set_color(cr, c);
  cairo_rectangle(cr, (double)x, (double)y, (double)w, (double)h);
  cairo_fill(cr);
  cairo_restore(cr);
}

void draw_line(cairo_t *cr, double x1, double y1, double x2, double y2, const gfx::LineStyle &line_style) {
  cairo_save(cr);
  set_line_style(cr, line_style);
  cairo_move_to(cr, x1, (double)y1);
  cairo_line_to(cr, x2, y2);
  cairo_stroke(cr);
  cairo_restore(cr);
}
  
static void gfx_draw_polygonal_chain(cairo_t *cr, const double (*p)[2], unsigned int n) {
  unsigned int i;
  cairo_move_to(cr, (double)p[0][0], (double)p[0][1]);
  for(i=1; i<n; i++) cairo_line_to(cr, (double)p[i][0], (double)p[i][1]);
}

static void gfx_draw_polygon(cairo_t *cr, const double (*p)[2], unsigned int n) {
  gfx_draw_polygonal_chain(cr, p, n);
  cairo_close_path(cr);
}
  
void draw_polygon(cairo_t *cr, const double (*p)[2], unsigned int n, const gfx::LineStyle &style=gfx::default_line_style) {
  cairo_save(cr);
  set_line_style(cr, style);
  gfx_draw_polygon(cr, p, n);
  cairo_stroke(cr);
  cairo_restore(cr);
}

void draw_polygonal_chain(cairo_t *cr, const double (*p)[2], unsigned int n, const gfx::LineStyle &style) {
  cairo_save(cr);
  set_line_style(cr, style);
  gfx_draw_polygonal_chain(cr, p, n);
  cairo_stroke(cr);
  cairo_restore(cr);
}

  
void draw_polygon_filled_with_color(cairo_t *cr, const double (*p)[2], unsigned int n, const gfx::Color &color) {
  cairo_save(cr);
  set_color(cr, color);
  gfx_draw_polygon(cr, p, n);
  cairo_fill(cr);
  cairo_restore(cr);
}
  
void draw_circle_filled_with_color(cairo_t *cr, double x, double y, double r, const gfx::Color &color) {
  cairo_save(cr);
  set_color(cr, color);
  cairo_new_path(cr);
  cairo_arc(cr, x, y, r, 0., 2.*M_PI);
  cairo_close_path(cr);
  cairo_fill(cr);
  cairo_restore(cr);
}
  
void draw_circle(cairo_t *cr, double x, double y, double r, const gfx::LineStyle &style) {
  cairo_save(cr);
  set_line_style(cr, style);
  cairo_new_path(cr);
  cairo_arc(cr, x, y, r, 0., 2.*M_PI);
  cairo_close_path(cr);
  cairo_stroke(cr);
  cairo_restore(cr);
}
  
void draw_point(cairo_t *cr, double x, double y, const gfx::Color &color) {
  draw_rectangle_filled_with_color(cr, x, y, 1, 1, color);
}
  
static void gfx_set_font(cairo_t *cr, gfx::Font font) {
  cairo_font_slant_t font_slant;
  cairo_font_weight_t font_weight;
  char const* font_family = font.family;
  switch(font.slant) {
  case gfx::FONT_SLANT_NORMAL: font_slant = CAIRO_FONT_SLANT_NORMAL; break;
  case gfx::FONT_SLANT_ITALIC: font_slant = CAIRO_FONT_SLANT_ITALIC; break;
  case gfx::FONT_SLANT_OBLIQUE: font_slant = CAIRO_FONT_SLANT_OBLIQUE; break;
  default: font_slant = CAIRO_FONT_SLANT_NORMAL;
  }
  switch(font.weight) {
  case gfx::FONT_WEIGHT_NORMAL: font_weight = CAIRO_FONT_WEIGHT_NORMAL; break;
  case gfx::FONT_WEIGHT_BOLD: font_weight = CAIRO_FONT_WEIGHT_BOLD; break;
  default: font_weight = CAIRO_FONT_WEIGHT_NORMAL;
  }
  if(font_family == nullptr || font_family[0] == '\0')
    font_family = "cairo:monospace";
    
  cairo_select_font_face(cr, font_family, font_slant, font_weight);
  cairo_set_font_size(cr, font.size);
}

static inline cairo_text_extents_t gfx_get_text_extents_cairo_t(cairo_t *cr, const char *str, const gfx::Font &font) {
  cairo_text_extents_t extents;
  gfx_set_font(cr, font);
  cairo_text_extents(cr, str, &extents);
  return extents;
}

void draw_text_centered(cairo_t *cr, double x, double y, const char *str, const gfx::Font &font) {
  cairo_text_extents_t extents = gfx_get_text_extents_cairo_t(cr, str, font);
  gfx_set_font(cr, font);
  cairo_move_to(cr, (double)x - extents.width/2, (double)y + extents.height/2);
  cairo_show_text(cr, str);
}

void render_cairo(cairo_t *canvas, const Kernel &core) {
  return render(canvas, core);
}
void render_objects_cairo(cairo_t *canvas, const Kernel &core) {
  return render_objects(canvas, core);
}
void render_background_cairo(cairo_t *canvas, const Kernel &core, double alpha) {
  return render_background(canvas, core, alpha);
}
