#ifndef __SVG_SCREENSHOT_HPP__
#define __SVG_SCREENSHOT_HPP__

#include "kernel.hpp"

void svg_screenshot(const Kernel&, const char*);
void svg_screenshot_time(const Kernel&, const char*);

class MultiScreenShot {
  void *context;
  const Kernel &core;
  double alpha;
  bool initial;
public:
  MultiScreenShot(const Kernel &core, double alpha, const char *filename);
  void capture();
  ~MultiScreenShot();
};

#endif // __SVG_SCREENSHOT_HPP__
