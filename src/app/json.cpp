#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <nlohmann/json.hpp>
#include "objects.hpp"
#include "kernel.hpp"
#include "effects.hpp"
#include "json.hpp"

using json = nlohmann::json;

static std::string to_str(const gfx::Color &c) {
  std::stringstream sstream;
  sstream << "#" << std::hex << (int)(255*c.r) << (int)(255*c.g) << (int)(255*c.b);
  if(c.a != 1.) sstream << (int)(255*c.a);
  return sstream.str();
}
static void to_color(const std::string s, gfx::Color &c) {
  if((s.length() != 7 && s.length() != 9) || s[0] != '#') {
    std::cerr << "Invalid color: " << s << std::endl;
    return;
  }
  c.r = std::stoi(s.substr(1, 2), nullptr, 16)/255.;
  c.g = std::stoi(s.substr(3, 2), nullptr, 16)/255.;
  c.b = std::stoi(s.substr(5, 2), nullptr, 16)/255.;
  if(s.length() == 9) c.a = std::stoi(s.substr(7, 2), nullptr, 16)/255.;
  else c.a = 1.;
}

static void to_json(json& j, const std::unique_ptr<DecayEffect>& obj);
static void to_json(json& j, const std::unique_ptr<CollisionEffect>&);

static void _to_json(json& j, const Movable& obj) {
  j = json{{"x", obj.x},
	   {"y", obj.y},
	   {"vx", obj.vx},
	   {"vy", obj.vy},
	   {"m_i", obj.m_i},
	   {"m_h", obj.m_h},
	   {"tau", obj.tau},
	   {"substantial", obj.substantial},
	   {"fixed", obj.fixed},
	   {"color", to_str(obj.color)},
	   {"label", obj.label},
	   {"on_decay", obj.on_decay},
	   {"on_collision", obj.on_collision},
  };
}
static void to_json(json& j, const Ball& obj) {
  _to_json(j, obj);
  j["r"] = obj.r;
}
static void to_json(json& j, const Rectangle& obj) {
  _to_json(j, obj);
  j["w"] = obj.w;
  j["h"] = obj.h;
}
class SerializeVisitor: public ConstVisitor {
  json& j;
public:
  virtual void visit(const Ball* obj) {
    j["__type__"] = "Ball";
    to_json(j, *obj);
  }
  virtual void visit(const Rectangle* obj) {
    j["__type__"] = "Rectangle";
    to_json(j, *obj);
  }
  virtual void visit(const Wall*) {}
  virtual ~SerializeVisitor() {}
  SerializeVisitor(json& j): j(j) {}
};
typedef Movable* Movable_p;
static void to_json(json& j, const Movable_p& obj) {
  SerializeVisitor vis(j);
  obj->accept_const(vis);
}
static void from_json(const json& j, std::unique_ptr<DecayEffect>& obj);
static void from_json(const json& j, std::unique_ptr<CollisionEffect>&);

static void _from_json(const json& j, Movable& obj) {
  std::string color;
  j.at("x").get_to(obj.x);
  j.at("y").get_to(obj.y);
  j.at("vx").get_to(obj.vx);
  j.at("vy").get_to(obj.vy);
  j.at("m_i").get_to(obj.m_i);
  j.at("m_h").get_to(obj.m_h);
  j.at("tau").get_to(obj.tau);
  j.at("substantial").get_to(obj.substantial);
  j.at("fixed").get_to(obj.fixed);
  j.at("color").get_to(color);
  to_color(color, obj.color);
  j.at("label").get_to(obj.label);
  j.at("on_decay").get_to(obj.on_decay);
  j.at("on_collision").get_to(obj.on_collision);
}
static void from_json(const json& j, Ball& obj) {
  _from_json(j, obj);
  j.at("r").get_to(obj.r);
}
static void from_json(const json& j, Rectangle& obj) {
  _from_json(j, obj);
  j.at("w").get_to(obj.w);
  j.at("h").get_to(obj.h);
}
static void from_json(const json& j, Movable*& obj) {
  std::string type;
  j.at("__type__").get_to(type);
  if(type == "Ball") {
    obj = new Ball();
    from_json(j, *(Ball*)obj);
  }
  else if(type == "Rectangle") {
    obj = new Rectangle();
    from_json(j, *(Rectangle*)obj);
  }
}

static void to_json(json& j, const Destroy& obj) {
  j = json{{"__name__", obj.name}};
}
static void to_json(json& j, const ExplosionRing& obj) {
  j = json{
	   {"__name__", obj.name},
	   {"n", obj.n},
	   {"phi", obj.phi},
	   {"tau", obj.tau},
	   {"r", obj.r},
	   {"substantial", obj.substantial},
  };
}
static void to_json(json& j, const Explosion& obj) {
  j = json{
	   {"__name__", obj.name},
	   {"n", obj.n},
	   {"tau", obj.tau},
	   {"r", obj.r},
	   {"substantial", obj.substantial},
  };
}

static void to_json(json& j, const DecayEffect& obj) {
  if(obj.name == Destroy::name)
    to_json(j, (Destroy&)obj);
  else if(obj.name == ExplosionRing::name)
    to_json(j, (ExplosionRing&)obj);
  else if(obj.name == Explosion::name)
    to_json(j, (Explosion&)obj);
}
static void to_json(json& j, const std::unique_ptr<DecayEffect>& obj) {
  if(!obj) return;
  to_json(j, *obj);
}

static void from_json(const json& j, std::unique_ptr<DecayEffect>& obj) {
  if(j.is_null())
    return;
  std::string name;
  j.at("__name__").get_to(name);
  if(name == Destroy::name)
    obj.reset(new Destroy{});
  else if(name == ExplosionRing::name)
    obj.reset(new ExplosionRing{j.at("n"), j.at("phi"), j.at("tau"), j.at("r"), j.at("substantial")});
  else if(name == Explosion::name)
    obj.reset(new Explosion{j.at("n"), j.at("tau"), j.at("r"), j.at("substantial")});
}
static void to_json(json& j, const Merge& obj) {
  j = json{
	   {"__name__", obj.name}
  };
}
static void to_json(json& j, const DisentangledCollisionEffect& obj) {
  j = json{
	   {"__name__", obj.name},
	   {"self", obj.self},
	   {"other", obj.other},
  };
}

static void to_json(json& j, const CollisionEffect& obj) {
  if(obj.name == Merge::name)
    to_json(j, (Merge&)obj);
  else if(obj.name == DisentangledCollisionEffect::name)
    to_json(j, (DisentangledCollisionEffect&)obj);
}
static void to_json(json& j, const std::unique_ptr<CollisionEffect>& obj) {
  if(!obj) return;
  to_json(j, *obj);
}

static void from_json(const json& j, std::unique_ptr<CollisionEffect>& obj) {
  if(j.is_null())
    return;
  std::string name;
  j.at("__name__").get_to(name);
  if(name == DisentangledCollisionEffect::name)
    obj.reset(new DisentangledCollisionEffect(j.at("self"), j.at("other")));
  else if(name == Merge::name)
    obj.reset(new Merge{});
}

static void to_json(json& j, const Kernel &obj) {
  json polygon;
  for(unsigned int i=0; i<obj.get_boundary().n; i++)
    polygon.push_back(json{{"x", obj.get_boundary().points[i][0]}, {"y", obj.get_boundary().points[i][1]}});

  std::vector<Movable*> located_objects = obj.movable_objects();
  located_objects.insert(located_objects.end(), obj.fixed_objects().begin(), obj.fixed_objects().end());
  j = json{
	   {"mu", obj.get_mu()},
	   {"G", obj.get_G()},
	   {"g_exp", obj.get_g_exp()},
	   {"boundary_polygon", polygon},
	   {"movables", located_objects},
	   {"g_x", obj.get_g_x()},
	   {"g_y", obj.get_g_y()},
  };
}

static void from_json(const json& j, Kernel& obj) {
  static double x;
  j.at("mu").get_to(x); obj.set_mu(x);
  j.at("G").get_to(x); obj.set_G(x);
  j.at("g_exp").get_to(x); obj.set_g_exp(x);
  j.at("g_x").get_to(x); obj.set_g_x(x);
  j.at("g_y").get_to(x); obj.set_g_y(x);
  std::vector<Movable*> located_objects;
  j.at("movables").get_to(located_objects);
  while(!located_objects.empty()) {
    auto o = located_objects.back();
    located_objects.pop_back();
    obj.add_movable(o);
  }

  json json_polygon = j.at("boundary_polygon");
  Polygon p{};

  p.set_n(json_polygon.size());
  unsigned int i = 0;
  for(json::iterator it = json_polygon.begin(); it != json_polygon.end(); ++it, ++i) {
    it->at("x").get_to(p.points[i][0]);
    it->at("y").get_to(p.points[i][1]);
  }
  obj.set_boundary(std::move(p));
}

void load_configuration(const std::string &filename, Kernel &core) {
  std::ifstream i(filename);
  json j;
  i >> j;
  from_json(j, core);
}

void save_configuration(const std::string &filename, const Kernel &core) {
  std::ofstream o(filename);
  json j;
  to_json(j, core);
  o << std::setw(2) << j << std::endl;
}
