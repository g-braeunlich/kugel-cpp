#include <time.h>
#include <memory.h>

#include "canvas_cairo_x11.hpp"
#include "svg_screenshot.hpp"
#include "input.hpp"
#include "json.hpp"


class EventHandlerVisitor {
public:
  Kernel &core;
  bool pause;
  bool quit;
  std::unique_ptr<MultiScreenShot> recorder;
  timespec ts;
  EventHandlerVisitor(Kernel &core): core{core}, pause{false}, quit{false}, recorder{nullptr}, ts{0, 5000000} {
  }
  void operator()(input::Event::Key key) {
    switch(key) {
    case input::Event::Key::SPACE:
      pause = !pause;
      return;
    case input::Event::Key::Q:
    case input::Event::Key::ESC:
      quit = true;
      return;
    case input::Event::Key::UP:
      ts.tv_nsec /= 2; return;
    case input::Event::Key::DOWN:
      ts.tv_nsec *= 2; return;
    case input::Event::Key::P:
      svg_screenshot_time(core, "/tmp/kugel");
      return;
    case input::Event::Key::R:
      if(recorder) recorder.reset(nullptr);
      else recorder.reset(new MultiScreenShot(core, 0.1, "/tmp/kugel"));
      return;
    default: return;
    }
  }
  void operator()(unsigned int button) { 
    switch(button) {
    case 1:
      break;
    }
  }
};

static void handle_event(EventHandlerVisitor &visitor, input::Event &&ev) {
  switch(ev.type) {
  case input::Event::KEY: return visitor(ev.key);
  case input::Event::BUTTON: return visitor(ev.button);
  default: return;
  }
}

int main(int argc, char **argv) {
  CanvasCairoX11 canvas;
  const unsigned int n = 20;
  Kernel core;
  if(argc == 2) load_configuration(argv[1], core);
  else Kernel::default_configuration(core, canvas.w, canvas.h, n);
  EventHandlerVisitor ev_handler(core);
  while(!ev_handler.quit) {
    if(!ev_handler.pause) {
      core.step();
      canvas.render(core);
      if(ev_handler.recorder) ev_handler.recorder->capture();
    }
    handle_event(ev_handler, canvas.check_event());
    nanosleep(&ev_handler.ts, NULL);
  }

  return 0;
}
