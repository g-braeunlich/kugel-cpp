#include <cmath>
#include <ctime>
#include <string>
#include "canvas_cairo.hpp"
#include "svg_screenshot.hpp"

extern "C" {
#include <cairo/cairo-svg.h>
}

static void boundary_box(const Polygon& p, double &x_min, double &x_max, double &y_min, double &y_max) {
  if(p.n == 0) x_min = x_max = y_min = y_max = NAN;
  else {
    x_min = x_max = p.points[0][0], y_min = y_max = p.points[0][1];
    for(unsigned int i=1; i<p.n; i++) {
      if(p.points[i][0] < x_min) x_min = p.points[i][0];
      if(p.points[i][0] > x_max) x_max = p.points[i][0];
      if(p.points[i][1] < y_min) y_min = p.points[i][1];
      if(p.points[i][1] > y_min) y_max = p.points[i][1];
    }
  }
}

static cairo_t* svg_from_boundary(const Polygon &boundary, const char *filename) {
  static double x_min, x_max, y_min, y_max, w, h;
  boundary_box(boundary, x_min, x_max, y_min, y_max);
  w = x_max - x_min;
  h = y_max - y_min;
  cairo_surface_t *sfc = cairo_svg_surface_create(filename, w, h);
  cairo_t *context = cairo_create(sfc);
  cairo_translate(context, -x_min, -y_min);
  return context;
}
static void svg_destroy(cairo_t *context) {
  cairo_surface_destroy(cairo_get_target(context));
  cairo_destroy(context);
}
void svg_screenshot(const Kernel &core, const char *filename) {
  cairo_t *context = svg_from_boundary(core.get_boundary(), filename);
  render_cairo(context, core);
  svg_destroy(context);
}

std::string date_filename(const char *filename) {
  time_t rawtime;
  tm *timeinfo;
  char buffer[19];
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, 19, "%Y-%m-%dT%H%M%S", timeinfo);
  auto filename_with_time = std::string{filename} + "-" + buffer + ".svg";
  return filename_with_time;
}

void svg_screenshot_time(const Kernel &core, const char *filename) {
  svg_screenshot(core, date_filename(filename).c_str());
}

MultiScreenShot::MultiScreenShot(const Kernel &core, double alpha, const char *filename) : core{core}, alpha{alpha}, initial{true} {
  context = svg_from_boundary(core.get_boundary(), date_filename(filename).c_str());
}
MultiScreenShot::~MultiScreenShot() {
  svg_destroy((cairo_t*)context);
}
void MultiScreenShot::capture() {
  if(initial) {
    initial = false;
    render_cairo((cairo_t*)context, core);
  }
  else {
    render_background_cairo((cairo_t*)context, core, alpha);
    render_objects_cairo((cairo_t*)context, core);
  }
}
