#ifndef __CANVAS_CAIRO_HPP__
#define __CANVAS_CAIRO_HPP__

#include "graphics.hpp"
#include "kernel.hpp"

typedef struct _cairo_surface cairo_surface_t;
typedef struct _cairo cairo_t;

struct CanvasCairo {
public:
  int w, h;
  cairo_t *ctx;
  cairo_surface_t *sfc;
  CanvasCairo() = delete;
  CanvasCairo(cairo_surface_t *_sfc, int w, int h);
  ~CanvasCairo();
  void render(const Kernel &core);
};

void render_cairo(cairo_t *canvas, const Kernel &core);
void render_objects_cairo(cairo_t *canvas, const Kernel &core);
void render_background_cairo(cairo_t *canvas, const Kernel &core, double alpha=1.);

#endif // __CANVAS_CAIRO_HPP__
