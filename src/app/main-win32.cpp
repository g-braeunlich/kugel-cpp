#include "json.hpp"
#include "canvas_cairo.hpp"

extern "C" {
#include <cairo/cairo-win32.h>
}

constexpr auto TITLE = TEXT("kugel");

class App {
public:
  Kernel core;
  bool pause;
};

static void get_dimensions(HWND window, int &w, int &h) {
  static RECT rect;
  GetClientRect(window, &rect);
  w = rect.right - rect.left;
  h = rect.bottom - rect.top;
}

static void
on_paint(HDC hdc, Kernel &core) {
  static int w, h;
  cairo_surface_t *surface = cairo_win32_surface_create(hdc);
  get_dimensions(WindowFromDC(hdc), w, h);
  CanvasCairo canvas{surface, w, h};
  canvas.render(core);
}


LRESULT CALLBACK
WndProc(HWND   window,
	UINT   message,
	WPARAM wParam,
	LPARAM lParam) {
  PAINTSTRUCT paint_struct;
  HDC dc;

  App &app = *reinterpret_cast<App*>(GetWindowLongPtr(window, GWLP_USERDATA));
  switch(message) {
  case WM_CHAR:
    switch(wParam) {
    case 'q':
    case 'Q':
      PostQuitMessage(0);
      return 0;
    case ' ':
      app.pause = !app.pause;
      return 0;
    }
    break;
  case WM_TIMER:
    if(app.pause) return 0;
    app.core.step();
    InvalidateRect(window, NULL, TRUE);
    return 0;
  case WM_ERASEBKGND:
    return TRUE; // No erasing, everything will be refreshed with WM_PAINT
  case WM_PAINT:
    if(app.core.get_boundary().n == 0)
      return 0;
    dc = BeginPaint(window, &paint_struct);
    on_paint(dc, app.core);
    EndPaint(window, &paint_struct);
    return 0;
  case WM_DESTROY:
    PostQuitMessage(0);
    return 0;
  default:
    break;
  }

  return DefWindowProc(window, message, wParam, lParam);
}

constexpr auto WINDOW_STYLE = WS_OVERLAPPEDWINDOW & ~(WS_MAXIMIZEBOX | WS_THICKFRAME);

INT WINAPI
WinMain(HINSTANCE hInstance,
	HINSTANCE,
	LPSTR     lpCmdLine,
	INT       /*iCmdShow*/) {
  HWND window;
  MSG message;
  WNDCLASS window_class;

  window_class.style = CS_HREDRAW | CS_VREDRAW;
  window_class.lpfnWndProc = WndProc;
  window_class.cbClsExtra = 0;
  window_class.cbWndExtra = 0;
  window_class.hInstance = hInstance;
  window_class.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  window_class.hCursor = LoadCursor(NULL, IDC_ARROW);
  window_class.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  window_class.lpszMenuName = NULL;
  window_class.lpszClassName = TITLE;

  RegisterClass(&window_class);

  window = CreateWindow(TITLE, /* Class name */
			TITLE, /* Window name */
			WINDOW_STYLE,
			CW_USEDEFAULT, CW_USEDEFAULT, /* initial position */
			CW_USEDEFAULT, CW_USEDEFAULT, /* initial size */
			NULL,	/* Parent */
			NULL,	/* Menu */
			hInstance,
			NULL); /* WM_CREATE lpParam */
  
  App app;
  app.pause = false;

  SetLastError(0);
  if(!SetWindowLongPtr(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(&app)) && GetLastError() != 0)
    return FALSE;

  SetTimer(window, 1, 20, NULL);
  ShowWindow(window, SW_MAXIMIZE);
  UpdateWindow(window);

  if(lpCmdLine[0] != '\0') load_configuration(lpCmdLine, app.core);
  else {
    const unsigned int n = 20;
    int w, h;
    get_dimensions(window, w, h);
    h -= 120;
    Kernel::default_configuration(app.core, w, h, n);
  }

  while(GetMessage(&message, NULL, 0, 0)) {
    TranslateMessage(&message);
    DispatchMessage(&message);
  }

  return message.wParam;
}
