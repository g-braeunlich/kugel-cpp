#ifndef __JSON_HPP__
#define __JSON_HPP__

#include <string>
#include "kernel.hpp"

void load_configuration(const std::string &filename, Kernel &core);
void save_configuration(const std::string &filename, const Kernel &core);

#endif //__JSON_HPP__
