#include "canvas_cairo.hpp"
#include "canvas_cairo_x11.hpp"
#include "input.hpp"

extern "C" {
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <cairo/cairo-xlib.h>
}

static void fullscreen(Display* dpy, Window win) {
  Atom atoms[2] = { XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", False), None };
  XChangeProperty(dpy, win, XInternAtom(dpy, "_NET_WM_STATE", False),
                  XA_ATOM, 32, PropModeReplace, (unsigned char*) atoms, 1);
}

static cairo_surface_t* x11_surface() {
  static int w, h;
  Display *dsp = XOpenDisplay(NULL);
  if(dsp == NULL) return NULL;
  int screen = DefaultScreen(dsp);
  Screen* scr = DefaultScreenOfDisplay(dsp);
  w = WidthOfScreen(scr);
  h = HeightOfScreen(scr);
  Drawable da = XCreateSimpleWindow(dsp, DefaultRootWindow(dsp), 0, 0, w, h, 0, 0, 0);
  fullscreen(dsp, da);
  XSelectInput(dsp, da, ButtonPressMask | KeyPressMask);
  XMapWindow(dsp, da);
  cairo_surface_t* sfc = cairo_xlib_surface_create(dsp, da, DefaultVisual(dsp, screen), w, h);
  cairo_xlib_surface_set_size(sfc, w, h);
  return sfc;
}

CanvasCairoX11::CanvasCairoX11() : CanvasCairo{x11_surface(), 0, 0} {
  w = cairo_xlib_surface_get_width(sfc);
  h = cairo_xlib_surface_get_height(sfc);
  dsp = cairo_xlib_surface_get_display(sfc);
}

CanvasCairoX11::~CanvasCairoX11() {
  XCloseDisplay((Display*)dsp);
}

static input::Event::Key to_key(KeySym sym) {
  // X11/keysmdef.h
  switch(sym) {
  case XK_space: return input::Event::Key::SPACE;
  case XK_q: return input::Event::Key::Q;
  case XK_p: return input::Event::Key::P;
  case XK_r: return input::Event::Key::R;
  case XK_Escape: return input::Event::Key::ESC;
  case XK_Up: return input::Event::Key::UP;
  case XK_Down: return input::Event::Key::DOWN;
  case XK_Left: return input::Event::Key::LEFT;
  case XK_Right: return input::Event::Key::RIGHT;
  default: return input::Event::Key::UNKNOWN;
  }
}

input::Event CanvasCairoX11::check_event() {
  Display* dpy = cairo_xlib_surface_get_display(sfc);
  KeySym sym;
  XEvent e;
    
  while(true) {
    if(!XPending(dpy))
      return input::Event{input::Event::Key::UNKNOWN, 0, input::Event::NONE};
    XNextEvent(dpy, &e);

    switch(e.type) {
    case ButtonPress:
      return input::Event{input::Event::Key::UNKNOWN, e.xbutton.button, input::Event::BUTTON};
    case KeyPress:
      sym = XLookupKeysym(&e.xkey, 0);
      return input::Event{to_key(sym), 0, input::Event::KEY};
    default:
      continue;
      //fprintf(stderr, "Dropping unhandled XEevent.type = %d.\n", e.type);
    }
  }
}
