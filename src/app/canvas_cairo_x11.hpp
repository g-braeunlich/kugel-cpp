#ifndef __CANVAS_CAIRO_X11_HPP__
#define __CANVAS_CAIRO_X11_HPP__

#include "input.hpp"
#include "canvas_cairo.hpp"

struct CanvasCairoX11 : public CanvasCairo {
protected:
  void *dsp;
public:
  CanvasCairoX11();
  ~CanvasCairoX11();
  
  input::Event check_event();
};

#endif // __CANVAS_CAIRO_X11_HPP__
