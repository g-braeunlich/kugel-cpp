#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include "effects.hpp"
#include "kernel.hpp"

const std::string DecayEffect::name = "decay_event";
DecayEffect::~DecayEffect() = default;

const std::string Destroy::name = "destroy";
Destroy::Destroy() {}
void Destroy::execute(Kernel &kernel, Movable &obj) {
  kernel.remove_object(&obj);
}
std::unique_ptr<DecayEffect> Destroy::copy() const {
  return std::unique_ptr<DecayEffect>(new Destroy(*this));
}

const std::string ExplosionRing::name = "explosion-ring";
std::unique_ptr<DecayEffect> ExplosionRing::copy() const {
  return std::unique_ptr<DecayEffect>(new ExplosionRing(*this));
}
ExplosionRing::ExplosionRing(unsigned int n, double phi, double tau, double r,
		     bool substantial): n(n), phi(phi), tau(tau), r(r), substantial(substantial) {}
void ExplosionRing::execute(Kernel &kernel, Movable &obj) {
  double r_k = r/2.+.001;
  double r = r_k*sin(M_PI/n);
  double v = sqrt(obj.vx*obj.vx+obj.vy*obj.vy);
  double m_i = obj.m_i/n, m_h = obj.m_h/n;
  double x = obj.x, y = obj.y, vx = obj.vx, vy = obj.vy;
  gfx::Color color = obj.color;
  kernel.remove_object(&obj);
  for(unsigned int i=0; i<n; i++) {
    Ball *B = new Ball();
    B->x = x+r_k*cos(phi+i*2.*M_PI/n);
    B->y = y+r_k*sin(phi+i*2.*M_PI/n);
    B->vx = vx+v*cos(phi+i*2.*M_PI/n);
    B->vy = vy+v*sin(phi+i*2.*M_PI/n);
    B->r = r;
    B->m_i = m_i;
    B->m_h = m_h;
    B->color = color;
    B->tau = tau;
    if(tau > 0.) B->on_decay = std::unique_ptr<DecayEffect>(new Destroy());
    B->substantial = substantial;
    kernel.add_movable(B);
  }
}

const std::string Explosion::name = "explosion";
std::unique_ptr<DecayEffect> Explosion::copy() const {
  return std::unique_ptr<DecayEffect>(new Explosion(*this));
}
Explosion::Explosion(unsigned int n, double tau, double r,
		       bool substantial): n(n), tau(tau), r(r), substantial(substantial) {}
  
void Explosion::execute(Kernel &kernel, Movable &obj) {
  static const double q = sqrt(.75), vx[] = {1., .5, -.5, -1., -.5, .5}, vy[] = {0., q, q, 0., -q, -q};
  static unsigned int i, _m, _n;
  i = 0;
  double _r;
  bool matrix_dot;
  std::vector<double> x, y;
  for(unsigned int rxr = 0; i<n; rxr++) {
    matrix_dot = false;
    _r = sqrt(rxr);
    for(_m = (unsigned int)(q*_r); _m<=(unsigned int)_r; _m++) {
      for(_n=0; _n<=_m; _n++)
	if(rxr == _n*_n+_n*_m+_m*_m){matrix_dot = true; break;}
      if(matrix_dot) break;
    }
    if(!matrix_dot) continue;
    i++;
    for(unsigned int j=0; j<6; j++) {
      x.push_back(vx[j]*_m+vx[(j+1)%6]*_n);
      y.push_back(vy[j]*_m+vy[(j+1)%6]*_n);
      if(_m != _n && _n != 0) {
	x.push_back(vx[j]*_n+vx[(j+1)%6]*_m);
	y.push_back(vy[j]*_n+vy[(j+1)%6]*_m);
      }
    }
  }
  double R = r/(2*_m+1)-.1;
  double m_i = obj.m_i/x.size();
  double m_h = obj.m_h/x.size();
  double v = sqrt(obj.vx*obj.vx+obj.vy*obj.vy);
  double x_0 = obj.x, y_0 = obj.y;
  gfx::Color color = obj.color;
  kernel.remove_object(&obj);
  for(auto xx=x.begin(), yy=y.begin(); xx!=x.end(); xx++, yy++) {
    Ball *ball = new Ball();
    ball->x = x_0+2.*R*(*xx);
    ball->y = y_0+2.*R*(*yy);
    ball->vx = v*(*xx);
    ball->vy = v*(*yy);
    ball->r = R;
    ball->m_i = m_i;
    ball->m_h = m_h;
    ball->color = color;
    ball->tau = tau;
    if(tau > 0.) {
      ball->on_decay = std::unique_ptr<DecayEffect>(new Destroy());
      //      ball.impact_passive = std::unique_ptr<DecayEffect>(new Destroy());
    }
    ball->substantial = substantial;
    kernel.add_movable(ball);
  }
}

void CollisionEffect::decompose(std::unique_ptr<DecayEffect>&, std::unique_ptr<DecayEffect>&) const {}
CollisionEffect::~CollisionEffect() = default;
const std::string CollisionEffect::name = "collision_effect";

const std::string DisentangledCollisionEffect::name = "disentangled_collision_effect";

DisentangledCollisionEffect::DisentangledCollisionEffect(std::unique_ptr<DecayEffect> &&self, std::unique_ptr<DecayEffect> &&other) : self{std::move(self)}, other{std::move(other)} {}
DisentangledCollisionEffect::DisentangledCollisionEffect(const DisentangledCollisionEffect& other) : self{other.self?other.self->copy():nullptr}, other{other.other?other.other->copy():nullptr} {}
std::unique_ptr<CollisionEffect> DisentangledCollisionEffect::copy() const {
  return std::unique_ptr<CollisionEffect>(new DisentangledCollisionEffect(*this));
}
std::unique_ptr<CollisionEffect> DisentangledCollisionEffect::copy_swapped() const {
  return std::unique_ptr<CollisionEffect>(new DisentangledCollisionEffect(this->other?this->other->copy():nullptr, this->self?this->self->copy():nullptr));
}

void DisentangledCollisionEffect::execute(Kernel &kernel, Movable &X, Movable &Y) {
  if(self != nullptr) self->execute(kernel, X);
  if(other != nullptr) other->execute(kernel, Y);
}
void DisentangledCollisionEffect::decompose(std::unique_ptr<DecayEffect>& A, std::unique_ptr<DecayEffect>& B) const {
  auto temp = self->copy();
  A.swap(temp);
  temp = other->copy();
  B.swap(temp);
}


const std::string Merge::name = "merge";
Merge::Merge() {}

void Merge::execute(Kernel &kernel, Movable &X, Movable &Y) {
  double M = X.m_h + Y.m_i;
  X.m_h = X.m_h + Y.m_h;
  X.x = (X.m_i*X.x + Y.m_i*Y.x) / M;
  X.y = (X.m_i*X.y + X.m_i*Y.y) / M;
  X.vx = (X.m_i*X.vx + Y.m_i*Y.vx) / M;
  X.vy = (X.m_i*X.vy + Y.m_i*Y.vy) / M;
  X.ax = (X.m_i*X.ax + Y.m_i*Y.ax) / M;
  X.ay = (X.m_i*X.ay + Y.m_i*Y.ay) / M;
  X.color.r = 0.5*(X.color.r + Y.color.r);
  X.color.g = 0.5*(X.color.g + Y.color.g);
  X.color.b = 0.5*(X.color.b + Y.color.b);
  X.m_i = M;
  kernel.remove_object(&Y);
}
std::unique_ptr<CollisionEffect> Merge::copy() const {
  return std::unique_ptr<CollisionEffect>(new Merge(*this));
}
std::unique_ptr<CollisionEffect> Merge::copy_swapped() const {
  return copy();
}
  
static std::unique_ptr<CollisionEffect> merge_collision_effects(const CollisionEffect& A, const CollisionEffect& B) {
  std::unique_ptr<DecayEffect> A1, A2, B1, B2;
  A.decompose(A1, B2);
  B.decompose(B1, A2);
  if(A1 || B1 || (A2 && B2)) { 
    if(A1) A2.swap(A1);
    if(B1) B2.swap(B1);
    return std::unique_ptr<CollisionEffect>(new DisentangledCollisionEffect(std::move(A2), std::move(B2)));
  }
  return A.copy();
}
std::unique_ptr<CollisionEffect> merge_collision_effects(const std::unique_ptr<CollisionEffect>& A, const std::unique_ptr<CollisionEffect>& B) {
  if(A && B) return merge_collision_effects(*A, *B);
  if(A) return A->copy();
  if(B) return B->copy_swapped();
  return std::unique_ptr<CollisionEffect>();
}
