#ifndef __KERNEL_HPP_
#define __KERNEL_HPP_

#include "objects.hpp"

#include <memory>
#include <vector>

class Polygon {
public:
  Polygon(unsigned int n);
  Polygon(double w, double h);
  Polygon(Polygon &&other);
  Polygon& operator=(Polygon&& other);
  Polygon();
  ~Polygon();
  double (*points)[2];
  unsigned int n;
  void set_n(unsigned int n);
};

class Kernel {
public:
  void step();
  void set_mu(double mu);
  void set_G(double G);
  void set_g_exp(double g_exp);
  void set_g_x(double g_x);
  void set_g_y(double g_y);
  void set_boundary(Polygon &&boundary);

  double get_mu() const;
  double get_G() const;
  double get_g_exp() const;
  double get_g_x() const;
  double get_g_y() const;
  const Polygon& get_boundary() const;

  void add_movable(Movable *o);
  void add_wall(Wall *o);
  void remove_object(Object *o);

  const std::vector<Movable*>& movable_objects() const;
  const std::vector<Movable*>& fixed_objects() const;

  Kernel(Polygon &&boundary, double mu = 0., double G = 0., double g_exp = -2.);
  Kernel();
  ~Kernel();

  static void default_configuration(Kernel &core, double w, double h, unsigned int n);
protected:
  struct Impl;
  std::unique_ptr<Impl> pimpl;
};

#endif // __KERNEL_HPP_
