#ifndef __EFFECTS_HPP__
#define __EFFECTS_HPP__

#include <memory>

class Kernel;
class Movable;

class DecayEffect {
public:
  static const std::string name;
  virtual void execute(Kernel &K, Movable &obj) = 0;
  virtual std::unique_ptr<DecayEffect> copy() const = 0;
  virtual ~DecayEffect();
};

class CollisionEffect {
public:
  static const std::string name;
  virtual void decompose(std::unique_ptr<DecayEffect> &A, std::unique_ptr<DecayEffect> &B) const;
  virtual void execute(Kernel &K, Movable &X, Movable &Y) = 0;
  virtual std::unique_ptr<CollisionEffect> copy() const = 0;
  virtual std::unique_ptr<CollisionEffect> copy_swapped() const = 0;
  virtual ~CollisionEffect() = 0;
};

class Destroy : public DecayEffect {
public:
  static const std::string name;
  Destroy();
  std::unique_ptr<DecayEffect> copy() const;
  
  void execute(Kernel &kernel, Movable &obj);
};

class ExplosionRing : public DecayEffect {
public:
  static const std::string name;
  unsigned int n;
  double phi, tau, r;
  bool substantial;
  
  ExplosionRing(unsigned int n, double phi, double tau, double r,
	    bool substantial);
  
  void execute(Kernel &kernel, Movable &obj);
  std::unique_ptr<DecayEffect> copy() const;
};

class Explosion : public DecayEffect {
public:
  static const std::string name;
  unsigned int n;
  double tau, r;
  bool substantial;
  
public:
  Explosion(unsigned int n, double tau, double r,
	     bool substantial);
  
  void execute(Kernel &kernel, Movable &obj);
  std::unique_ptr<DecayEffect> copy() const;
};

class DisentangledCollisionEffect : public CollisionEffect {
public:
  static const std::string name;
  std::unique_ptr<DecayEffect> self, other;
  std::unique_ptr<CollisionEffect> copy() const;
  std::unique_ptr<CollisionEffect> copy_swapped() const;
public:
  DisentangledCollisionEffect(const DisentangledCollisionEffect&);
  DisentangledCollisionEffect(std::unique_ptr<DecayEffect> &&self, std::unique_ptr<DecayEffect> &&other);
  void decompose(std::unique_ptr<DecayEffect> &A, std::unique_ptr<DecayEffect> &B) const;
  void execute(Kernel &kernel, Movable &X, Movable &Y);
};

class Merge : public CollisionEffect {
public:
  Merge();
  static const std::string name;
  std::unique_ptr<CollisionEffect> copy() const;
  std::unique_ptr<CollisionEffect> copy_swapped() const;
  
  void execute(Kernel &kernel, Movable &X, Movable &Y);
};

std::unique_ptr<CollisionEffect> merge_collision_effects(const std::unique_ptr<CollisionEffect>& A, const std::unique_ptr<CollisionEffect>& B);

#endif // __EFFECTS_HPP__
