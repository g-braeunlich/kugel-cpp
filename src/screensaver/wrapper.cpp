#include "canvas_X11.hpp"
#include "kernel.hpp"


extern "C"
{
#include "wrapper.h"

  struct _kernel {
    Kernel core;
    _kernel(double w, double h, unsigned int n) {
      Kernel::default_configuration(core, w, h, n);
    }
  };

  kernel* default_kernel(double w, double h, unsigned int n) {
    kernel *core = new kernel(w, h, n);
    return core;
  }
  void kernel_destroy(kernel *core) {
    delete core;
  }

  void kernel_step(kernel *core) {
    core->core.step();
  }
  void kernel_render(const kernel *core, canvas *bmp) {
    CanvasX11 &canvas = *(CanvasX11*)bmp;
    canvas.render(core->core);
  }
  
  canvas *canvas_new(Display *display, Window win, GC gc, double w, double h) {
    return (canvas*)new CanvasX11(display, win, gc, w, h);
  }
  void canvas_destroy(canvas *c) {
    delete (CanvasX11*)c;
  }
}
