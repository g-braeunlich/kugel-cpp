#include "canvas_X11.hpp"
#include "graphics-render.hpp"
extern "C" {
#include <string.h>
}
#include <cmath>

CanvasX11::CanvasX11(Display *display, Window win, GC gc, double w, double h)
  : display{display}, win{win}, gc{gc}, w{w}, h{h} {}
CanvasX11::~CanvasX11() {
  XCloseDisplay(display);
}

void CanvasX11::render(const Kernel &core) {
  ::render(*this, core);
  XFlush(display);
}


static void set_color(CanvasX11 &backend, const gfx::Color &clr) {
  static XColor color;
  const static unsigned short max_clr_val = 37856;
  color.red = clr.r*max_clr_val; color.green = clr.g*max_clr_val; color.blue = clr.b*max_clr_val;
  color.flags = DoRed | DoGreen | DoBlue;
  XAllocColor(backend.display, DefaultColormap(backend.display, DefaultScreen(backend.display)), &color);

  XSetForeground(backend.display, backend.gc, color.pixel);
}

static void set_line_style(CanvasX11 &backend, const gfx::LineStyle &style) {
  set_color(backend, style.color);
  XSetLineAttributes(backend.display, backend.gc, style.width, (style.n_dashes != 0)?LineOnOffDash:LineSolid, CapNotLast, JoinMiter);
  if(style.n_dashes != 0)
    {
      char *dash_lengths = new char[style.n_dashes];
      for(int i=0; i<style.n_dashes; i++) dash_lengths[i] = style.dash_lengths[i];
      XSetDashes(backend.display, backend.gc, style.dash_offset, dash_lengths, style.n_dashes);
    }
}
  
void draw_rectangle(CanvasX11 &backend, double x, double y, double w, double h, const gfx::LineStyle &line_style) {
  set_line_style(backend, line_style);
  XDrawRectangle(backend.display, backend.win, backend.gc, x, y, w, h);
}
  
  
void draw_rectangle_filled_with_color(CanvasX11 &backend, double x, double y, double w, double h, const gfx::Color &c) {
  set_color(backend, c);
  XFillRectangle(backend.display, backend.win, backend.gc, x, y, w, h);
}

void draw_line(CanvasX11 &backend, double x1, double y1, double x2, double y2, const gfx::LineStyle &line_style) {
  set_line_style(backend, line_style);
  XDrawLine(backend.display, backend.win, backend.gc, x1, y1, x2, y2);
}
  
void draw_polygon(CanvasX11 &backend, const double (*p)[2], unsigned int n, const gfx::LineStyle &style=gfx::default_line_style) {
  XPoint *points = new XPoint[n+1];
  for(unsigned int i=0; i<n; i++) points[i].x = p[i][0], points[i].y = p[i][1];
  points[n].x = p[0][0], points[n].y = p[0][1];
  set_line_style(backend, style);
  XDrawLines(backend.display, backend.win, backend.gc, points, n+1, CoordModeOrigin);
  delete[] points;
}
void draw_polygonal_chain(CanvasX11 &backend, const double (*p)[2], unsigned int n, const gfx::LineStyle &style) {
  XPoint *points = new XPoint[n];
  for(unsigned int i=0; i<n; i++) points[i].x = p[i][0], points[i].y = p[i][1];
  set_line_style(backend, style);
  XDrawLines(backend.display, backend.win, backend.gc, points, n, CoordModeOrigin);
  delete[] points;
}
  
void draw_polygon_filled_with_color(CanvasX11 &backend, const double (*p)[2], unsigned int n, const gfx::Color &color) {
  XPoint *points = new XPoint[n];
  for(unsigned int i=0; i<n; i++) points[i].x = p[i][0], points[i].y = p[i][1];
  set_color(backend, color);

  XFillPolygon(backend.display, backend.win, backend.gc, points, n, Complex, CoordModeOrigin);
  delete[] points;
}
  
void draw_circle_filled_with_color(CanvasX11 &backend, double x, double y, double r, const gfx::Color &color) {
  set_color(backend, color);
  XFillArc(backend.display, backend.win, backend.gc, x-r, y-r, 2*r, 2*r, 0, 360*64);
}
  
void draw_circle(CanvasX11 &backend, double x, double y, double r, const gfx::LineStyle &style) {
  set_line_style(backend, style);
  XDrawArc(backend.display, backend.win, backend.gc, x-r, y-r, 2*r, 2*r, 0, 360*64);
}
  
void draw_point(CanvasX11 &backend, double x, double y, const gfx::Color &color) {
  set_color(backend, color);
  XDrawPoint(backend.display, backend.win, backend.gc, x, y);
}
  
void draw_text_centered(CanvasX11 &backend, double x, double y, const char *str, const gfx::Font &font) {
  static int nchars;
  nchars = strlen(str);
  char const* font_family = font.family;
  if(font_family == nullptr || font_family[0] == '\0')
    font_family = "fixed";
  ::Font _font = XLoadFont(backend.display, font_family);
  XSetFont(backend.display, backend.gc, _font);
  set_color(backend, font.color);
  int direction, font_ascent, font_descent;
  XCharStruct overall_return;
  XQueryTextExtents(backend.display, _font, str, nchars, &direction, &font_ascent, &font_descent, &overall_return);
  XDrawString(backend.display, backend.win, backend.gc, x - font_ascent/2, y - font_descent/2, str, nchars);
  XUnloadFont(backend.display, _font);
}
