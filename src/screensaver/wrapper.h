#ifndef __WRAPPER_H__
#define __WRAPPER_H__

#include <X11/Xlib.h>

typedef struct _kernel kernel;
typedef struct _canvas canvas;

kernel* default_kernel(double w, double h, unsigned int n);
void kernel_destroy(kernel *core);
void kernel_step(kernel *core);
void kernel_render(const kernel *core, canvas *bmp);

canvas *canvas_new(Display *display, Window root, GC gc, double w, double h);
void canvas_destroy(canvas *c);

#endif // __WRAPPER_H__
