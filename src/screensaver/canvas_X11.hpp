#ifndef __CANVAS_X11_HPP__
#define __CANVAS_X11_HPP__

#include "graphics.hpp"
#include "kernel.hpp"
extern "C" {
#include <X11/Xlib.h>
}

struct CanvasX11 {
  Display *display;
  Window win;
  GC gc;
  double w, h;
  CanvasX11(Display*, Window, GC, double, double);
  ~CanvasX11();
  void render(const Kernel &core);
};

#endif // __CANVAS_X11_HPP__
