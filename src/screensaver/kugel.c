#include <stdlib.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xatom.h>
#include "wrapper.h"

static Window VirtualRootWindowOfScreen(Screen *screen) {
  static Screen *save_screen = NULL;
  static Window win = (Window)0;
  
  if(screen != save_screen) {
    Display *dpy = DisplayOfScreen(screen);
    Atom __SWM_VROOT = None;
    unsigned int i;
    Window rootReturn, parentReturn, *children;
    unsigned int numChildren;
      
    win = RootWindowOfScreen(screen);
      
    /* go look for a virtual root */
    __SWM_VROOT = XInternAtom(dpy, "__SWM_VROOT", False);
    if(XQueryTree(dpy, win, &rootReturn, &parentReturn,
		  &children, &numChildren)) {
      for(i=0; i<numChildren; i++) {
	Atom actual_type;
	int actual_format;
	unsigned long nitems, bytesafter;
	Window *newRoot = NULL;
	      
	if(XGetWindowProperty(dpy, children[i],
			      __SWM_VROOT, 0, 1, False, XA_WINDOW,
			      &actual_type, &actual_format,
			      &nitems, &bytesafter,
			      (unsigned char**)&newRoot) == Success
	   && newRoot) {
	  win = *newRoot;
	  break;
	}
      }
      if(children) XFree((char*)children);
    }
    save_screen = screen;
  }
  return win;
}

int main() {
  Display *dpy;
  Window win;
  GC gc;

  dpy = XOpenDisplay(getenv("DISPLAY"));
  win = VirtualRootWindowOfScreen(DefaultScreenOfDisplay(dpy));
  gc = XCreateGC(dpy, win, 0, NULL);

  /* set foreground color */
  XSetForeground(dpy, gc, WhitePixelOfScreen(DefaultScreenOfDisplay(dpy)) );
  double w = 100., h = 100.;
  canvas *cv = canvas_new(dpy, win, gc, w, h);
  kernel *core = default_kernel(w, h, 5);
  while(1) {
    kernel_step(core);
    kernel_render(core, cv);
    usleep(10000);
  }

  canvas_destroy(cv);
  kernel_destroy(core);
}
