#ifndef __RELATION_MANAGER_HPP__
#define __RELATION_MANAGER_HPP__

#include <list>
#include <unordered_map>
#include <functional>

typedef std::function<void()> Remover;

class RelationLink;

class ObjectLink {
public:
  typedef std::list<RelationLink*> Container;

  ~ObjectLink();
  Remover add_relation(RelationLink* relation_link) {
    relations.push_back(relation_link);
    return [&rels = relations, target = --relations.end()]() {rels.erase(target);};
  }
  void add_remover(Remover remover) {removers.push_back(remover);};
protected:
  Container relations;
  std::list<Remover> removers;
};

class RelationLink {
public:
  RelationLink(Remover del, ObjectLink &o1, ObjectLink &o2)
    : remove_1(o1.add_relation(this)),
      remove_2(o2.add_relation(this)),
      relation_remover(del)
  {}
  ~RelationLink();
protected:
  Remover remove_1, remove_2;
  Remover relation_remover;
};

ObjectLink::~ObjectLink() {
  while(!relations.empty())
    delete relations.back();
  for(auto i=removers.begin(); i!=removers.end(); i++) (*i)();
}

template<typename ValueType>
class RelationManager {
public:
  RelationManager() = default;
  ~RelationManager() = default;
  void add_object(const ValueType&);
  void add_relation(const ValueType&, const ValueType&, Remover);
  void add_remover(const ValueType& o, Remover r) { object_links[o].add_remover(r); }
  void remove_object(const ValueType&);
private:
  std::unordered_map<ValueType, ObjectLink> object_links;
};

template<typename ValueType>
void RelationManager<ValueType>::add_object(const ValueType &object) {
  object_links.insert(std::pair<ValueType,ObjectLink>(object, ObjectLink()));
}

template<typename ValueType>
void RelationManager<ValueType>::add_relation(const ValueType& o1, const ValueType& o2, Remover del)
{ new RelationLink(del, object_links[o1], object_links[o2]); }
template<typename ValueType>
void RelationManager<ValueType>::remove_object(const ValueType& object)
{ object_links.erase(object); }

RelationLink::~RelationLink() {
  remove_1();
  remove_2();
  relation_remover();
}

#endif // __RELATION_MANAGER_HPP__
