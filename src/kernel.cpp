#include <cmath>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <tuple>
#include <algorithm>
#include <typeindex>
#include <typeinfo>
#include <cstdlib>

#include "kernel.hpp"
#include "objects.hpp"
#include "relation_manager.hpp"
#include "multi_dispatch.hpp"
#include "numerics.hpp"

const static double EPS = 1.E-10;

class PossibleEvent {
public:
  virtual void execute() = 0;
  virtual double t_occurence() = 0;
  virtual ~PossibleEvent() = 0;
};
PossibleEvent::~PossibleEvent() {}

class Interaction {
public:
  virtual void act() = 0;
  virtual ~Interaction() = 0;
};
Interaction::~Interaction() {}

template<typename T>
struct FreeingAllocator : public std::allocator<T*> {
  void deallocate(T** o, std::size_t s) {
    std::free(*o);
    std::allocator<T*>::deallocate(o, s);
  }
};

typedef std::vector<PossibleEvent*, FreeingAllocator<PossibleEvent>> EventContainer;
typedef std::vector<Interaction*, FreeingAllocator<Interaction>> InteractionContainer;

struct Kernel::Impl {
  Kernel &core;
  double mu;
  double G, g_exp;
  double g_x, g_y;

  Polygon boundary;

  EventContainer possible_events;
  InteractionContainer interactions;
  std::unordered_set<Object*, std::hash<Object*>, std::equal_to<Object*>, FreeingAllocator<Object>> objects;
  std::vector<Wall*> boundary_walls;
  std::vector<Movable*> moving_objects;
  std::vector<Movable*> fixed_objects;
  RelationManager<Object*> relation_manager;

  Impl(Kernel &core, Polygon &&boundary, double mu, double G, double g_exp) : core{core}, mu{mu}, G{G}, g_exp{g_exp}, g_x{0.}, g_y{0.}, boundary{std::move(boundary)} {
    make_walls();
  }

  void add_interaction(Object*, Object*, Interaction*);
  void add_event(Object*, PossibleEvent*);
  void add_event(Object*, Object*, PossibleEvent*);

  void friction(Movable &obj);
  void uniform_field_force(Movable &obj);

  template <typename C, typename T>
  void add_unary(C&, Object*, const T&);

  template <typename C, typename T>
  void add_binary(C&, Object*, Object*, const T&);
  struct EventTable;
  struct InteractionTable;

  void add_object(Object*);
  void add_movable(Movable*);
  void add_boundary_wall(Wall*);
  void remove_object(Object*);

  void make_walls();
};

Kernel::Kernel(Polygon &&boundary, double mu, double G, double g_exp) : pimpl{std::make_unique<Impl>(*this, std::move(boundary), mu, G, g_exp)} {}
Kernel::Kernel() : pimpl{std::make_unique<Impl>(*this, Polygon{}, 0., 0., 0.)} {}
Kernel::~Kernel() = default;

void Kernel::set_mu(double mu) { pimpl->mu = mu;}
void Kernel::set_G(double G) { pimpl->G = G;}
void Kernel::set_g_exp(double g_exp) { pimpl->g_exp = g_exp;}
void Kernel::set_g_x(double g_x) { pimpl->g_x = g_x;}
void Kernel::set_g_y(double g_y) { pimpl->g_y = g_y;}
void Kernel::set_boundary(Polygon &&boundary) {
  pimpl->boundary = std::move(boundary);
  pimpl->make_walls();
}
double Kernel::get_mu() const { return pimpl->mu;}
double Kernel::get_G() const { return pimpl->G;}
double Kernel::get_g_exp() const { return pimpl->g_exp;}
double Kernel::get_g_x() const { return pimpl->g_x;}
double Kernel::get_g_y() const { return pimpl->g_y;}
const Polygon& Kernel::get_boundary() const {return pimpl->boundary;}

const std::vector<Movable*>& Kernel::movable_objects() const {
  return pimpl->moving_objects;
}
const std::vector<Movable*>& Kernel::fixed_objects() const {
  return pimpl->fixed_objects;
}


void Kernel::step() {
  static double dt, T, t_s;
  static PossibleEvent *occurring_event;
  T = 1;
  while(T > 0.) {
    occurring_event = nullptr;
    dt = T;
    for(auto evt = pimpl->possible_events.begin(); evt != pimpl->possible_events.end(); evt++) {
      t_s = (*evt)->t_occurence();
      if(-EPS <= t_s && t_s < dt) {
	occurring_event = *evt;
	dt = t_s;
      }
    }
    T -= dt;
    // Leapfrog method - kick-drift-kick form:
    // kick:
    for_each(pimpl->moving_objects.begin(), pimpl->moving_objects.end(), [](auto& i) { (*i).accellerate(0.5*dt);});
    // drift:
    for(auto obj = pimpl->moving_objects.begin(); obj != pimpl->moving_objects.end(); obj++)
      (*obj)->move(dt);
    if(occurring_event != nullptr)
      occurring_event->execute();
    // kick:
    for_each(pimpl->interactions.begin(), pimpl->interactions.end(), [](auto& i) { (*i).act();});
    for_each(pimpl->moving_objects.begin(), pimpl->moving_objects.end(), [](auto& i) { (*i).accellerate(0.5*dt);});
  }
  for(auto obj = pimpl->moving_objects.begin(); obj != pimpl->moving_objects.end(); obj++) {
    pimpl->friction(**obj);
    pimpl->uniform_field_force(**obj);
  }
}

typedef std::tuple<std::type_index, std::type_index> type_pair;

template <typename T_A, typename T_B>
class PossibleBinaryEvent : public PossibleEvent {
protected:
  T_A &A;
  T_B &B;
public:
  PossibleBinaryEvent(T_A &_A, T_B&_B): A(_A), B(_B) {}
  ~PossibleBinaryEvent() = default;
};

template <typename T>
class PossibleUnaryEvent : public PossibleEvent {
protected:
  T &A;
public:
  PossibleUnaryEvent(T &_A): A(_A) {}
  ~PossibleUnaryEvent() = default;
};

inline double r_exp_generic(double exp, double dx, double dy)
{ return pow(dx*dx + dy*dy, .5*(exp-1.)); }

template<int EXP>
double r_exp(double dx, double dy)
{ return r_exp_generic(dx, dy, EXP); }

template<> double r_exp<-2>(double dx, double dy) {
  double rxr = dx*dx + dy*dy;
  return rxr * sqrt(rxr);
}
template<> double r_exp<-1>(double dx, double dy)
{ return 1./(dx*dx + dy*dy); }
template<> double r_exp<0>(double dx, double dy)
{ return 1./sqrt(dx*dx + dy*dy); }
template<> double r_exp<1>(double dx, double dy)
{ return 0.5/log(dx*dx + dy*dy); }

template<bool unilateral=false, int EXP=-2>
class CentralForce : public Interaction {
  static_assert(EXP <= 0,
		"Only negative numbers or 0 are allowed for central force exponents!");
protected:
  Movable &A;
  Movable &B;
public:
  double G;
  void act();  
  CentralForce(double _G, Movable &_A, Movable&_B): A(_A), B(_B), G(_G) {}
  ~CentralForce() = default;
};

template<bool unilateral=false>
class CentralForceGeneralExponent : public Interaction {
protected:
  Movable &A;
  Movable &B;
public:
  double G, exp;
  void act();  
  CentralForceGeneralExponent(double _G, double _exp, Movable &_A, Movable&_B): A(_A), B(_B), G(_G), exp(_exp) {}
  ~CentralForceGeneralExponent() = default;
};



inline static void central_force_unilateral(Movable &X, double dx, double dy, double f) {
  X.ax = f*dx;
  X.ay = f*dy;
}

template<bool unilateral>
inline static void central_force_pair_interaction(Movable &A, Movable &B, double dx, double dy, double F);
template<>
inline void central_force_pair_interaction<false>(Movable &A, Movable &B, double dx, double dy, double F) {
  central_force_unilateral(A, dx, dy, -F*B.m_h);
  central_force_unilateral(B, dx, dy, F*A.m_h);
}
template<>
inline void central_force_pair_interaction<true>(Movable& A, Movable &B, double dx, double dy, double F) {
  central_force_unilateral(B, dx, dy, F*A.m_h);
}

template<bool unilateral>
void CentralForceGeneralExponent<unilateral>::act() {
  static double dx, dy;
  dx = A.x - B.x;
  dy = A.y - B.y;
  central_force_pair_interaction<unilateral>(A, B, dx, dy, G/r_exp_generic(exp, dx, dy));
}
template<bool unilateral, int EXP>
void CentralForce<unilateral, EXP>::act() {
  static double dx, dy;
  dx = A.x - B.x;
  dy = A.y - B.y;
  central_force_pair_interaction<unilateral>(A, B, dx, dy, G/r_exp<EXP>(dx, dy));
}

template<bool unilateral=false>
Interaction* central_force_by_exponent(double G, double exponent, Movable &A, Movable &B) {
  if(round(exponent) == exponent)
    switch(int(exponent)) {
    case 0: return new CentralForce<unilateral, 0>(G, A, B);
    case -1: return new CentralForce<unilateral, -1>(G, A, B);
    case -2: return new CentralForce<unilateral, -2>(G, A, B);
    default: break;
    }
  return new CentralForceGeneralExponent<unilateral>(G, exponent, A, B);
}

void Kernel::Impl::friction(Movable &obj) {
  if(mu == 0.) return;
  double vxv = (obj.vx)*(obj.vx)+(obj.vy)*(obj.vy);
  if(vxv < mu*mu)
    obj.vx = obj.vy = 0.;
  else if(vxv != 0.)
    {
      double v = sqrt(vxv);
      obj.vx *= (v-mu)/v;
      obj.vy *= (v-mu)/v;
    }
}
void Kernel::Impl::uniform_field_force(Movable &obj) {
  obj.vx += g_x;
  obj.vy += g_y;
}

template<typename T_A, typename T_B>
class ElasticCollision: public PossibleBinaryEvent<T_A, T_B> {
public:
  ElasticCollision(T_A &A, T_B &B) : PossibleBinaryEvent<T_A, T_B>(A, B) {}
  void execute();
  double t_occurence();
  ~ElasticCollision() = default;
};

template<typename T_A, typename T_B>
class CollisionWithEffect: public ElasticCollision<T_A, T_B> {
  std::unique_ptr<CollisionEffect> effect;
  Kernel &core;
public:
  CollisionWithEffect(T_A &A, T_B &B, std::unique_ptr<CollisionEffect> &&effect, Kernel &core) : ElasticCollision<T_A, T_B>(A, B), effect{std::move(effect)}, core{core} {}
  void execute();
  ~CollisionWithEffect() = default;
};

static inline void mu_movable(const Movable &X, const Movable &Y, double &mu_X, double &mu_Y) {
  static double k;
  switch(((std::isinf(X.m_i) || X.fixed) << 1) + (std::isinf(Y.m_i) || Y.fixed)) {
  case 0: // m_X, m_Y < inf
    k = 2./(X.m_i + Y.m_i);
    mu_X = X.m_i * k;
    mu_Y = Y.m_i * k;
    break;
  case 1: // m_Y == inf
    mu_X = 0., mu_Y = 2.;
    break;
  case 2: // m_X == inf
    mu_X = 2., mu_Y = 0.;
    break;
  case 3: // m_X, m_Y == inf
    mu_X = 1., mu_Y = 1.;
  }
}

static void point_collision(Movable &X, Movable &Y, double dx, double dy) {
  static double mu_A, mu_B, k;
  mu_movable(X, Y, mu_B, mu_A);
  k = (dx*(X.vx - Y.vx) + dy*(X.vy - Y.vy))/(dx*dx + dy*dy);
  X.vx -= k * mu_A * dx;
  X.vy -= k * mu_A * dy;
  Y.vx += k * mu_B * dx;
  Y.vy += k * mu_B * dy;
}

template<> void ElasticCollision<Ball, Ball>::execute() {
  point_collision(A, B, A.x - B.x, A.y - B.y);
}
static double t_occurence_points_no_accel(double dx, double dy, double dvx, double dvy, double R) {
  /**
   * Time until point A reaches B in distance R, or -1, if
   * they do not meet
   * \| dr + t dv \|^2 = R^2  <=>  t^2 dv^2 + t 2 <dr, dv> + dr^2 - R^2 = 0
   * => t = -<dr, dv>/dv^2 +- \sqrt{<dr, dv>^2 - dv^2 (dr^2-R^2)}/dv^2 = (dr^2-R^2)/(-<dr, dv> -+ \sqrt{<dr, dv>^2 - dv^2 (dr^2-R^2)})
   */
  static double S, dvQ, D;
  S = -dx*dvx - dy*dvy;	// - dr * dv
  if(S <= 0.) return -1.;	// Distance increases between the points.
  dvQ = dvx*dvx + dvy*dvy;	// dv^2
  // b = |dx*dvy - dy*dvx|/|dv| is impact parameter
  D = -(dx*dvy - dy*dvx)*(dx*dvy - dy*dvx) + R*R*dvQ;
  if(D < 0.) return -1.;	// b > R => balls do not touch
  //return (S-sqrt(D))/dvQ;
  return (dx*dx + dy*dy - R*R)/(S+sqrt(D));
  // Solution t = (S+sqrt(D))/dvQ would be after a move through
}

static double t_occurence_points(double dx, double dy, double dvx, double dvy, double dax, double day, double R) {
  // \| dx + t dv + 1/2 t^2 da \|^2 = R^2
  // <=> 0 = t^4/4 \|da\|^2 + t^3 <da, dv> + (<da, dx> + \|dv\|^2) t^2 + 2 <dx, dv> t + \|dx\|^2 - R^2
  static double p[5];
  if(fabs(dax) < EPS && fabs(day) < EPS)
    return t_occurence_points_no_accel(dx, dy, dvx, dvy, R);
  p[4] = 0.25 * (dax*dax + day*day);
  p[3] = dax*dvx + day*dvy;
  p[2] = (dax*dx + day*dy) + (dvx*dvx+dvy*dvy);
  p[1] = 2.*(dx*dvx + dy*dvy);
  p[0] = dx*dx + dy*dy - R*R;
  if(fabs(p[0]) < EPS && p[1] <= 0.) return 0.;
  return zero_p4(p, EPS);
}
template<> double ElasticCollision<Ball, Ball>::t_occurence()
/**
 * Time until collision of ball A with ball B, or -1, if they
 * do not collide
 */
{
  static double R;
  if(!A.substantial || !B.substantial) R = A.r>B.r?A.r:B.r;
  else R = A.r + B.r;
  return t_occurence_points(B.x - A.x, B.y - A.y, B.vx - A.vx, B.vy - A.vy, B.ax - A.ax, B.ay - A.ay, R);
}

template<> void ElasticCollision<Rectangle, Rectangle>::execute()
/* ______
 * |\  i/|
 * | \ /~i
 * |  x  |
 * | / \j|
 * |/ ~j\|
 * -------
 */
{
  static double nx[2][2] = {{ 0., 1.}, {-1., 0.}};
  static double ny[2][2] = {{-1., 0.}, { 0., 1.}};
  static double dx, dy, k, mu_A, mu_B;
  static bool i, j;
  dx = B.x - A.x;
  dy = B.y - A.y;
  mu_movable(A, B, mu_A, mu_B);
  i = A.w * dy - A.h * dx > 0.;
  j = A.w * dy + A.h * dx > 0.;
  dx = nx[i][j];
  dy = ny[i][j];
  k = (dx*(A.vx - B.vx) + dy*(A.vy - B.vy));
  A.vx += k * mu_A * dx;
  A.vy += k * mu_A * dy;
  B.vx -= k * mu_B * dx;
  B.vy -= k * mu_B * dy;
}

static double t_occurence_wall(double delta, double v_n, double a_n) {
  // <x + vt + 1/2 a t^2, n> - (d+r) = 0
  // delta := r + d - x_n
  // => t = -(v_n +- sqrt(v_n^2 + 2 a_n delta))/a_n
  //      = 2 delta/(v_n -+ sqrt(v_n^2 + 2 a_n delta))
  // a_n t + v_n < 0
  // => s sqrt(v_n^2 + 2 a_n delta) > 0
  // s = 1
  // => t = -(v_n + sqrt(v_n^2 + 2 a_n delta))/a_n
  //      = 2 delta/(v_n - sqrt(v_n^2 + 2 a_n delta))
  static double D, q;
  D = v_n*v_n + 2.*a_n*delta;
  if(D < 0. || (v_n == 0. && a_n == 0)) return -1.;
  q = v_n + copysign(sqrt(D), v_n);
  if(v_n < 0.) return 2.*delta/q;
  else return -q/a_n;
}
static double t_occurence_rect(double dx, double dy, double dvx, double dvy, double dax, double day, double w, double h, double r = 0.) {
  static double t, _t;
  t = t_occurence_wall(w - dx, dvx, dax);
  if(fabs(dy + t*(dvy + 0.5*t*day)) > h-r) t = -1.;
  _t = t_occurence_wall(w + dx, -dvx, -dax);
  if(_t >= 0. && fabs(dy + _t*(dvy + 0.5*_t*day)) <= h-r && (t < 0. || _t < t)) t = _t;
  _t = t_occurence_wall(h - dy, dvy, day);
  if(_t >= 0. && fabs(dx + _t*(dvx + 0.5*_t*dax)) <= w-r && (t < 0. || _t < t)) t = _t;
  _t = t_occurence_wall(h + dy, -dvy, -day);
  if(_t >= 0. && fabs(dx + _t*(dvx + 0.5*_t*dax)) <= w-r && (t < 0. || _t < t)) t = _t;
  return t;
}
  
template<> double ElasticCollision<Rectangle, Rectangle>::t_occurence()
/**
 * Time until collision of rectangle A with rectangle B, or -1, if they do not
 * collide
 */
{
  static double w, h;
  if(!A.substantial || !B.substantial) {
    w = fmax(A.w, B.w);
    h = fmax(A.h, B.h);
  }
  else {
    w = A.w + B.w;
    h = A.h + B.h;
  }
  // max( dx(t)/w, dy(t)/h ) = 1
  w *= 0.5;
  h *= 0.5;
  return t_occurence_rect(B.x - A.x, B.y - A.y, B.vx - A.vx, B.vy - A.vy, B.ax - A.ax, B.ay - A.ay, 0.5*w, 0.5*h);
}
template<> double ElasticCollision<Ball, Rectangle>::t_occurence() {
  static double w, h, t, _t, dx, dy, dvx, dvy, dax, day;
  if(!A.substantial || !B.substantial) {
    w = fmax(A.r, B.w);
    h = fmax(A.r, B.h);
  }
  else {
    w = A.r + 0.5*B.w;
    h = A.r + 0.5*B.h;
  }
  dx = B.x - A.x, dy = B.y - A.y;
  dvx = B.vx - A.vx, dvy = B.vy - A.vy;
  dax = B.ax - A.ax, day = B.ay - A.ay;
  t = t_occurence_rect(dx, dy, dvx, dvy, dax, day, w, h, A.r);
  if(t >= 0.) return t;
  for(double sx=-0.5; sx<=0.5; sx+=1.)
    for(double sy=-0.5; sy<=0.5; sy+=1.) {
      _t = t_occurence_points(dx + sx*B.w, dy + sy*B.h, dvx, dvy, dax, day, A.r);
      if(_t >= 0. && (t < 0. || _t < t))
	t = _t;
    }
  return t;
}
template<> void ElasticCollision<Ball, Rectangle>::execute() {
  static double dx, dy, Vx2, mu_A, mu_B;
  dx = A.x - B.x, dy = A.y - B.y;
  if(fabs(dy) <= 0.5 * B.h) {
    mu_movable(A, B, mu_A, mu_B);
    Vx2 = (A.vx * mu_A + B.vx * mu_B);
    A.vx = Vx2 - A.vx;
    B.vx = Vx2 - B.vx;
    return;
  }
  if(fabs(dx) <= 0.5 * B.w) {
    mu_movable(A, B, mu_A, mu_B);
    Vx2 = (A.vy * mu_A + B.vy * mu_B);
    A.vy = Vx2 - A.vy;
    B.vy = Vx2 - B.vy;
    return;
  }
  point_collision(A, B, dx + 0.5 * copysign(B.w, dx), dy + 0.5 * copysign(B.w, dy));
}

static void wall_collision(Movable &X, Wall &Y) {
  static double k;
  k = 2.*(Y.nx*X.vx+Y.ny*X.vy);
  X.vx -= k*Y.nx;
  X.vy -= k*Y.ny;
}

template<> void ElasticCollision<Ball, Wall>::execute() {
  wall_collision(A, B);
}
template<> double ElasticCollision<Ball, Wall>::t_occurence() {
/**
 * Time until collision of ball K and wall G or negative integer, if they do not touch.
 */
  // <x + vt + 1/2 a t^2, n> - (d+r) = 0
  // => t = -(v_n +- sqrt(v_n^2 + 2 a_n (r+d-x_n)))/a_n
  //      = 2 (r+d-x_n)/(v_n -+ sqrt(v_n^2 + 2 a_n (r+d-x_n)))
  static double delta, v_n, x_n, a_n;
  v_n = A.vx*B.nx + A.vy*B.ny;
  x_n = A.x*B.nx + A.y*B.ny;
  a_n = A.ax*B.nx + A.ay*B.ny;
  delta = B.d+A.r-x_n;
  return t_occurence_wall(delta, v_n, a_n);
}
template<> double ElasticCollision<Rectangle, Wall>::t_occurence() {
  static double delta, v_n, x_n, a_n, r;
  v_n = A.vx*B.nx + A.vy*B.ny;
  x_n = A.x*B.nx + A.y*B.ny;
  a_n = A.ax*B.nx + A.ay*B.ny;
  r = 0.5*(  B.nx*copysign(A.w, B.nx)
	   + B.ny*copysign(A.h, B.ny));
  delta = B.d+r-x_n;
  return t_occurence_wall(delta, v_n, a_n);
}
template<> void ElasticCollision<Rectangle, Wall>::execute() {
  wall_collision(A, B);
}

template<class T1, class T2> void CollisionWithEffect<T1, T2>::execute() {
  effect->execute(core, this->A, this->B);
}


class Decay: public PossibleUnaryEvent<Movable> {
public:
  void execute() {
    ev.execute(kernel, A);
  }
  double t_occurence() { return A.tau; }
  Decay(Movable &A, Kernel &kernel) : PossibleUnaryEvent<Movable>(A), kernel(kernel), ev(*A.on_decay) {}
protected:
  Kernel &kernel;
  DecayEffect &ev;
};


void Kernel::add_movable(Movable *o) {
  if(o->tau > 0. && o->on_decay != nullptr)
    pimpl->add_event(o, new Decay(*o, *this));
  pimpl->add_movable(o);
}
void Kernel::add_wall(Wall *o) {
  pimpl->add_object(o);
}

void Kernel::Impl::add_object(Object *o) {
  relation_manager.add_object(o);
  for(auto obj=objects.begin(); obj!=objects.end(); obj++) {
    dispatch<Impl::EventTable, void, Impl&>(*this, *o, **obj);
    dispatch<Impl::InteractionTable, void, Impl&>(*this, *o, **obj);
  }
  objects.emplace(o);
}
void Kernel::Impl::add_boundary_wall(Wall *o) {
  add_object(o);
  boundary_walls.emplace_back(o);
}
void Kernel::Impl::add_movable(Movable *o) {
  add_object(o);
  if(!o->fixed) add_unary(moving_objects, o, o);
  else add_unary(fixed_objects, o, o);
}
void Kernel::Impl::remove_object(Object *o) {
  relation_manager.remove_object(o);
  objects.erase(o);
}
void Kernel::remove_object(Object *o) {
  pimpl->remove_object(o);
}

struct Kernel::Impl::EventTable {
  template<typename T1, typename T2>
  static void dispatch_t(Kernel::Impl &core, T1& obj1, T2 &obj2) {
    if(obj1.fixed && obj2.fixed) return;
    auto effect = merge_collision_effects(obj1.on_collision, obj2.on_collision);
    PossibleEvent* event;
    if(effect) event = new CollisionWithEffect<T1, T2>(obj1, obj2, std::move(effect), core.core);
    else event = new ElasticCollision<T1, T2>(obj1, obj2);
    core.add_event(&obj1, &obj2, event);
  }
  template<typename T1, typename T2>
  static void dispatch_t_unilateral(Kernel::Impl &core, T1& obj1, T2 &obj2) {
    if(!obj1.fixed)
      core.add_event(&obj1, &obj2, new ElasticCollision<T1, T2>(obj1, obj2));
  }
  
  static void dispatch(Kernel::Impl &core, Ball& obj1, Ball &obj2)
  { dispatch_t(core, obj1, obj2); }
  static void dispatch(Kernel::Impl &core, Ball& obj1, Rectangle &obj2)
  { dispatch_t(core, obj1, obj2); }
  static void dispatch(Kernel::Impl &core, Rectangle& obj1, Ball &obj2)
  { dispatch_t(core, obj2, obj1); }
  static void dispatch(Kernel::Impl &core, Rectangle& obj1, Rectangle &obj2)
  { dispatch_t(core, obj1, obj2); }
  static void dispatch(Kernel::Impl &core, Ball& obj1, Wall &obj2)
  { dispatch_t_unilateral(core, obj1, obj2); }
  static void dispatch(Kernel::Impl &core, Wall& obj1, Ball &obj2)
  { dispatch_t_unilateral(core, obj2, obj1); }
  static void dispatch(Kernel::Impl&, Wall&, Wall&) {}
  static void dispatch(Kernel::Impl &core, Rectangle& obj1, Wall &obj2)
  { dispatch_t_unilateral(core, obj1, obj2); }
  static void dispatch(Kernel::Impl &core, Wall& obj1, Rectangle &obj2)
  { dispatch_t_unilateral(core, obj2, obj1); }
};

struct Kernel::Impl::InteractionTable {
  static void central_force_generator_movable(Movable &A, Movable &B, Kernel::Impl &core) {
    if(A.m_h != 0. && B.m_h != 0. && core.G != 0. && !A.fixed && !B.fixed) {
      Interaction *action;
      if(A.fixed)
	action = central_force_by_exponent<true>(core.G, core.g_exp, A, B);
      else if(B.fixed)
	action = central_force_by_exponent<true>(core.G, core.g_exp, B, A);
      else
	action = central_force_by_exponent<false>(core.G, core.g_exp, A, B);
      core.add_interaction(&A, &B, action);
    }
  }
  static void dispatch(Kernel::Impl &core, Ball& obj1, Ball &obj2)
  { central_force_generator_movable(obj1, obj2, core); }
  static void dispatch(Kernel::Impl &core, Ball& obj1, Rectangle &obj2)
  { central_force_generator_movable(obj1, obj2, core); }
  static void dispatch(Kernel::Impl &core, Rectangle& obj1, Ball &obj2)
  { central_force_generator_movable(obj1, obj2, core); }
  static void dispatch(Kernel::Impl &core, Rectangle& obj1, Rectangle &obj2)
  { central_force_generator_movable(obj1, obj2, core); }
  static void dispatch(Kernel::Impl&, Ball&, Wall&) {}
  static void dispatch(Kernel::Impl&, Wall&, Ball&) {}
  static void dispatch(Kernel::Impl&, Wall&, Wall&) {}
  static void dispatch(Kernel::Impl&, Rectangle&, Wall&) {}
  static void dispatch(Kernel::Impl&, Wall&, Rectangle&) {}
};

template <typename C, typename T>
Remover remover(C& c, const T &x) {
  return [&cnt=c, tgt=x]() {
	   auto it = std::find(cnt.begin(), cnt.end(), tgt);
	   if(it != cnt.end())
	     cnt.erase(it);
	 };
}

template <typename C, typename T>
void Kernel::Impl::add_unary(C &container, Object *o, const T& r) {
  container.push_back(r);
  relation_manager.add_remover(o, remover(container, r));
}

template <typename C, typename T>
void Kernel::Impl::add_binary(C& container, Object* o1, Object* o2, const T& r) {
  container.push_back(r);
  relation_manager.add_relation(o1, o2, remover(container, r));
}

void Kernel::Impl::add_interaction(Object* o1, Object* o2, Interaction *I)
{ add_binary(interactions, o1, o2, I); }
void Kernel::Impl::add_event(Object* o1, Object* o2, PossibleEvent* E)
{ add_binary(possible_events, o1, o2, E); }
void Kernel::Impl::add_event(Object* o, PossibleEvent* E)
{ add_unary(possible_events, o, E); }

void Kernel::Impl::make_walls() {
  while(!boundary_walls.empty()) {
    Object* o = boundary_walls.back();
    boundary_walls.pop_back();
    remove_object(o);
  }
  if(boundary.n < 2) return;
  for(unsigned int i=1; i<boundary.n; i++)
    add_boundary_wall(new Wall(boundary.points[i-1][0], boundary.points[i-1][1], boundary.points[i][0], boundary.points[i][1]));
  add_boundary_wall(new Wall(boundary.points[boundary.n-1][0], boundary.points[boundary.n-1][1], boundary.points[0][0], boundary.points[0][1]));
}

Polygon::Polygon(unsigned int n) : points{new double[n][2]}, n{n} {}
Polygon::Polygon(double w, double h) : points{new double[4][2]}, n{4} {
  points[0][0] = 0.;
  points[0][1] = 0.;
  points[1][0] = w;
  points[1][1] = 0.;
  points[2][0] = w;
  points[2][1] = h;
  points[3][0] = 0.;
  points[3][1] = h;
}
Polygon::Polygon() : points{nullptr}, n{0} {}
Polygon::Polygon(Polygon &&other) : points{other.points}, n{other.n} { other.points = nullptr;}
Polygon& Polygon::operator=(Polygon&& other) {
  if(this == &other) return *this;
  if(points != nullptr) delete[] points;
  points = other.points;
  other.points = nullptr;
  n = other.n;
  return *this;
}
void Polygon::set_n(unsigned int _n) {
  if(points != nullptr) delete[] points;
  n = _n;
  points = new double[n][2];
}

Polygon::~Polygon() {
  if(points != nullptr)
    delete[] points;
}

static double rnd() {
  return (double)rand()/RAND_MAX;
}

void Kernel::default_configuration(Kernel &core, double w, double h, unsigned int n) {
  const static double r = 10.;
  const static double v_max = 1.;
  const static double m_max = 1000.;	// maximal mass for objects
  const static double m_min = 100.;	// minimal mass for objects
  core.set_boundary(Polygon(w, h));
  for(unsigned int i=0; i<n; i++) {
      Ball *B = new Ball();
      B->x = r + rnd()*(w-2.*r);
      B->y = r + rnd()*(h-2.*r);
      B->r = r;
      B->vx = (2.*rnd()-1.)*v_max;
      B->vy = (2.*rnd()-1.)*v_max;
      B->m_i = m_min + rnd()*(m_max-m_min);
      B->m_h = 0.;
      B->fixed = false;
      B->substantial = true;
      B->tau = 0.;
      B->color = gfx::Color{.r=rnd(), .g=rnd(), .b=rnd(), .a=1.};
      
      core.add_movable(B);
    }
}

