#ifndef __GRAPHICS_RENDER_HPP__
#define __GRAPHICS_RENDER_HPP__

#include "graphics.hpp"
#include "kernel.hpp"
#include "multi_dispatch.hpp"


template<class T>
void render_objects(T &canvas, const Kernel &core) {
  for(auto obj=core.movable_objects().begin(); obj!=core.movable_objects().end(); ++obj)
    draw_object(canvas, **obj);
  for(auto obj=core.fixed_objects().begin(); obj!=core.fixed_objects().end(); ++obj)
    draw_object(canvas, **obj);
}
template<class T>
void render_background(T &canvas, const Kernel &core, double alpha=1.) {
  draw_polygon_filled_with_color(canvas, core.get_boundary().points, core.get_boundary().n, gfx::Color{1., 1., 1., alpha});
}

template<class T>
void render(T &canvas, const Kernel &core) {
  render_background(canvas, core, 1.);
  draw_polygon(canvas, core.get_boundary().points, core.get_boundary().n);
  render_objects(canvas, core);
}

constexpr gfx::Font label_font = gfx::Font{ .slant = gfx::FONT_SLANT_NORMAL, .weight = gfx::FONT_WEIGHT_NORMAL, .family = "Helvetica", .size = 1, .color = gfx::black};

template<class T>
struct DrawTable {
  static void dispatch(T &canvas, Ball &obj) {
    if(obj.r == 0) draw_point(canvas, obj.x, obj.y, obj.color);
    else draw_circle_filled_with_color(canvas, obj.x, obj.y, obj.r, obj.color);
    static gfx::Font font = label_font;
    if(!obj.label.empty()) {
      font.size = label_font.size * obj.r;
      font.color = (gfx::get_L(obj.color) <= .75?gfx::white:gfx::black);
      draw_text_centered(canvas, obj.x, obj.y, obj.label.c_str(), font);
    }
  }
  static void dispatch(T &canvas, Rectangle &obj) {
    const double p[][2] = {
			   {obj.x-obj.w/2, obj.y-obj.h/2},
			   {obj.x+obj.w/2, obj.y-obj.h/2},
			   {obj.x+obj.w/2, obj.y+obj.h/2},
			   {obj.x-obj.w/2, obj.y+obj.h/2}};
    draw_polygon_filled_with_color(canvas, p, 4, obj.color);
  }
  static void dispatch(T&, Wall&) {}
};

template<class T>
void draw_object(T &canvas, Movable &obj) {
  dispatch<DrawTable<T>, void, T&, Object>(canvas, obj);
}


#endif // __GRAPHICS_RENDER_HPP__
