#!/usr/bin/gawk -f

# Removes transparent rectangles and blends to underlaying paths
# Use on svg created by kugel (trace)

BEGIN {
    alpha = 1.;
}

{ a[i++] = $0 }

END {
    for(j=i-1;j>=0;j--) {
        if(match(a[j], /^<rect x="0" y="0".*fill-opacity:(0.[0-9]*);/, m_alpha)) {
            alpha = alpha * (1-m_alpha[1]);
            a[j] = "";
}
        if(match(a[j], /^(<path style=".*;fill:rgb\()([0-9]+\.?[0-9]*)%,([0-9]+\.?[0-9]*)%,([0-9]+\.?[0-9]*)%(\);.*\/>)$/, m_rgb)) {
            r = m_rgb[2]*alpha + 100*(1-alpha);
            g = m_rgb[3]*alpha + 100*(1-alpha);
            b = m_rgb[4]*alpha + 100*(1-alpha);
            a[j] = m_rgb[1] r "%," g "%," b "%" m_rgb[5];
        }
    }
    for(j=0;j<i;j++) {
        print a[j];
    }
}

