\documentclass[a4paper,11pt,fleqn]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage[pdftex]{hyperref}

\author{G. Bräunlich}
\title{kugel - Physical model}

\fancyhf{}
\fancyhead[RO,LE]{Kugel}
\fancyfoot[C]{\thepage}
%\renewcommand{\headrulewidth{0pt}}
%\renewcommand{\footrulewidth{0pt}}

% E: Even Page 
% O: Odd Page
% L: Left 
% C: Center
% R: Right
%\graphicspath{{figs/}{doc/figs/}}
\graphicspath{{figs/}}


\begin{document}
\maketitle

\newpage

\tableofcontents

\newpage

\pagestyle{fancy}

\section{Notation}
\setlength{\unitlength}{1cm}
\begin{picture}(10, 7)
  \put(3,4){\circle*{0.2}}
  \put(7,5){\circle*{8}}
  \put(3,4){\vector(4,-1){2}}
  \put(7,5){\vector(-4,-3){1.8}}
  \put(2.5,3){Ball A}
  \put(2.5,2.5){Position: $\boldsymbol{x}_A$}
  \put(2.5,2){Mass: $m_A$}
  \put(2.5,1.5){Radius: $r_A$}
  \put(6.5,4){Ball B}
  \put(6.5,3.5){Position: $\boldsymbol{x}_B$}
  \put(6.5,3){Mass: $m_B$}
  \put(6.5,2.5){Radius: $r_B$}
  \put(4,4){$\boldsymbol{v}_A$}
  \put(5.5,4.5){$\boldsymbol{v}_B$}
\end{picture}

\paragraph{Definitions}
\begin{align*}
  \boldsymbol{\Delta x} & = \boldsymbol{x}_A - \boldsymbol{x}_B &
  \textnormal{(Distance)}\\
  \boldsymbol{\Delta v} & = \boldsymbol{v}_A - \boldsymbol{v}_B &
  \textnormal{(Relative velocity)}\\
  \boldsymbol{p}_A & = m_A \boldsymbol{v}_A,\qquad \boldsymbol{p}_B = m_B \boldsymbol{v}_B & \textnormal{(Momenta)}
\end{align*}

\section{Determining the minimal distance of balls along their trajectories}

Remark: This section is actually not used in the simulation.
\\

Time $t_{\textrm{min}}$ and distance $d_{\textrm{min}}$ are determined
as follows:

\begin{equation}
  \label{eq:t_min}
  t_{\textrm{min}} = -\frac{\langle \boldsymbol{\Delta v}, \boldsymbol{\Delta x} \rangle}{\|\boldsymbol{\Delta v}\|^2}
\end{equation}

\begin{equation}
  \label{eq:d_min}
  d_{\textrm{min}}
  = \left\| \boldsymbol{\Delta x} - \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|} \langle \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|},
  \boldsymbol{\Delta x} \rangle \right\|
  = \| \boldsymbol{\Delta x} \times \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|} \|
\end{equation}

\subsection{Derivation}
The distance of the trajectories of the balls
$\boldsymbol{x}_A(t) = \boldsymbol{x}_A + t\boldsymbol{v}_A$ and
$\boldsymbol{x}_B(t) = \boldsymbol{x}_B + t\boldsymbol{v}_B$ is
minimal, if and only if the square of the distance is minimal.\\
Therefore the condition is:
\begin{align*}
  0 & = \frac{\mathrm{d}}{\mathrm{d}t} \left[ \| \boldsymbol{x}_A(t) - \boldsymbol{x}_B(t) \|^2 \right] = 2 \langle \boldsymbol{\Delta v}, t\boldsymbol{\Delta v} + \boldsymbol{\Delta x} \rangle \\
    & = 2 t\|\boldsymbol{\Delta v}\|^2 + 2 \langle \boldsymbol{\Delta v}, \boldsymbol{\Delta x} \rangle
\end{align*}

Solving for $t$, yields \eqref{eq:t_min}.\\

$d_{\textrm{min}}$ is obtained by substituting $t = t_{\textrm{min}}$
into the distance
$\| \boldsymbol{x}_A(t) - \boldsymbol{x}_B(t) \|$:
\begin{equation*}
  d_{\textrm{min}} = \| t\boldsymbol{\Delta v} + \boldsymbol{\Delta x} \|
  = \left\| \boldsymbol{\Delta x} - \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|} \langle \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|},
  \boldsymbol{\Delta x} \rangle \right\|
  = \| \boldsymbol{\Delta x} \times \tfrac{\boldsymbol{\Delta
      v}}{\|\boldsymbol{\Delta v}\|} \|.
\end{equation*}
This is \eqref{eq:d_min}.

\section{Determining the time until the next collision}

The time $t_\mathrm{in}$ until two non overlapping balls touch and
the time $t_\mathrm{out}$ until two overlapping balls disconnect are
given by:

\begin{subequations}
  \begin{align}
    \label{eq:t_in}
    t_{\mathrm{in}} &= \frac{\|\boldsymbol{\Delta x}\|^2 - (r_A + r_B)^2}{\sqrt{(r_A + r_B)^2 \|\boldsymbol{\Delta
          v}\|^2 -\| \boldsymbol{\Delta x} \times \boldsymbol{\Delta
          v}\|^2} - \langle \boldsymbol{\Delta x}, \boldsymbol{\Delta
        v} \rangle }, \\
    t_{\mathrm{out}} &= \frac{\sqrt{(r_A + r_B)^2 \|\boldsymbol{\Delta
          v}\|^2 -\| \boldsymbol{\Delta x} \times \boldsymbol{\Delta v}\|^2} - \langle \boldsymbol{\Delta x},
      \boldsymbol{\Delta v} \rangle }{\|\boldsymbol{\Delta v}\|^2}.
  \end{align}
\end{subequations}

\subsection{Derivation}

\begin{figure}
  \centering
  \includegraphics[width=0.25\linewidth]{t_collision}
  \caption{Changing to relative coordinates, the geometric problem
    reduces to find the intersection points of a straight line with a circle.}
  \label{fig:t_collision}
\end{figure}

The condition, that two balls touch is:
\begin{equation*}
\|\boldsymbol{x}_A + t\boldsymbol{v}_A - ( \boldsymbol{x}_B + t\boldsymbol{v}_B) \| = r_A + r_B,
\end{equation*}
or squared:
\begin{equation*}
  \|\boldsymbol{x}_A -\boldsymbol{x}_B + t(\boldsymbol{v}_A -
  \boldsymbol{v}_B) \|^2
  = (r_A + r_B)^2.
\end{equation*}

Using the abbreviations (change to relative coordinates, see Figure~\ref{fig:t_collision})
$\boldsymbol{\Delta x} = \boldsymbol{x}_A - \boldsymbol{x}_B$ and
$\boldsymbol{\Delta v} = \boldsymbol{v}_A - \boldsymbol{v}_B$,
an expansion gives:

\begin{equation*}
  \|\boldsymbol{\Delta x}\|^2 - (r_A + r_B)^2 + t^2
  \|\boldsymbol{\Delta v}\|^2 + 2t \langle \boldsymbol{\Delta x},
  \boldsymbol{\Delta v} \rangle = 0.
\end{equation*}

Solving for $t$:

\begin{equation}
  \label{eq:t_general}
  t_\pm = \frac{- \langle \boldsymbol{\Delta x}, \boldsymbol{\Delta v} \rangle \pm \sqrt{D}}{\|\boldsymbol{\Delta v}\|^2},
\end{equation}
with
\begin{equation*}
  D = (r_A + r_B)^2 \|\boldsymbol{\Delta v}\|^2 + 
  \langle \boldsymbol{\Delta x},
  \boldsymbol{\Delta v} \rangle^2
  - \|\boldsymbol{\Delta x}\|^2\|\boldsymbol{\Delta v}\|^2.
\end{equation*}

As $\sqrt{D} \geq 0$, it is clear that $t_- \leq t_+$ and thus
$t_\mathrm{in} = t_-$ and $t_\mathrm{out} = t_+$.

As we are interested in solutions in the future, we will restrict to
the case where the scalar product in \eqref{eq:t_general} is negative.
In this case, for numerical reasons (to avoid numerical cancellation) it is
beneficial to rewrite the final
solution as

$$
t = \frac{- \langle \boldsymbol{\Delta x}, \boldsymbol{\Delta
    v} \rangle - \sqrt{D}}{
  \|\boldsymbol{\Delta v}\|^2}
= \frac{1}{\|\boldsymbol{\Delta v}\|^2}\frac{\langle \boldsymbol{\Delta x}, \boldsymbol{\Delta
    v} \rangle^2 - D}{\sqrt{D} - \langle \boldsymbol{\Delta x}, \boldsymbol{\Delta v} \rangle}.
$$

\eqref{eq:t_in} follows from using the following identity (for an explanation, see
Figure~\ref{fig:scalar_cross}) in the discriminant,

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{scalar_cross}
  \caption{Projection of vector $\boldsymbol{b}$ onto $\boldsymbol{a}$
    and its orthogonal complement. Pythagoras yields \eqref{eq:scalar_cross}}
  \label{fig:scalar_cross}
\end{figure}

\begin{equation}
  \label{eq:scalar_cross}
  \|\boldsymbol{a} \times \boldsymbol{b} \|^2 + \langle
  \boldsymbol{a}, \boldsymbol{b}\rangle^2 = \|\boldsymbol{a}\|^2 \|\boldsymbol{b}\|^2
\end{equation}

\subsection{Collision detection with acceleration}

As we are using the leap frog method to integrate the equations of
motion in the presence of motion, we have to consider acceleration:

In the leap frog method, the steps involved are as follows:

\begin{enumerate}
  \item $\boldsymbol{v} \rightarrow \boldsymbol{v} + \tfrac{1}{2}\Delta t
    \boldsymbol{a}$ (kick)
  \item $\boldsymbol{x} \rightarrow \boldsymbol{x} + \Delta t
    \boldsymbol{v}$ (drift)
  \item $\boldsymbol{v} \rightarrow \boldsymbol{v} + \tfrac{1}{2}\Delta t
    \boldsymbol{a}$ (kick)
\end{enumerate}
That means, at the end of one step, a ball will have moved
\begin{equation*}
  \boldsymbol{\Delta x} = \Delta t \boldsymbol{v} +
  \tfrac{1}{2}\Delta t^2 \boldsymbol{a},
\end{equation*}
where $\boldsymbol{v}$ and $\boldsymbol{a}$ are the values at the
beginning of the step.

If within a step, a collision occurs, the goal is to determine the
time step $\Delta t$ at which the collision happens.

Therefore in the condition
\begin{equation*}
  \|\boldsymbol{x}_A(t) -\boldsymbol{x}_B(t) \|^2
  = (r_A + r_B)^2
\end{equation*}
from the previous subsection, we also have to introduce the quadratic
term $\tfrac{1}{2} t^2 \boldsymbol{a}$:

\begin{equation*}
  \boldsymbol{x}_{A,B}(t) = \boldsymbol{x}_{A,B} +
  \boldsymbol{v}_{A,B} t + \tfrac{1}{2} t^2 \boldsymbol{a}_{A,B}.
\end{equation*}

This now leads to the problem of finding the zeros of a polynomial $p(t)$ of
degree 4 (the geometric problem is to intersect a parabola with a
circle, see Figure~\ref{fig:t_collision_a})
\begin{figure}
  \centering
  \includegraphics[width=0.25\linewidth]{t_collision_a}
  \caption{In relative coordinates, the geometric problem
    reduces to find the intersection points of a parabola with a
    circle, with up to 4 possible solutions.}
  \label{fig:t_collision_a}
\end{figure}

Even though, there are explicit formulae to determine the zeros of a
polynomial of degree 4, it seems to be numerically more accurate to
use an iterative method. Newton's method only converges to one of the
solutions (depending on the initial value). We are interested in
the first time $t$ after $t = 0$ where the parabola
intersects the circle (this can be either $t_1$ or $t_3$ in
Figure~\ref{fig:t_collision_a}, depending on whether
$t_1 < 0$ or $t \geq 0$).

Here, Bairstow's method is used, i.e., the polynomial is factorized into
two polynomials of degree 2 (this is always possible for polynomials of
degree 4 with real coefficients, because each complex zero is
accompanied by a complex conjugated zero).

Once we have a factorization $p(t) = c p_1(t) p_2(t)$, for
\begin{align*}
  p_1(t) &= t^2 + r_1 t + q_1 \\
  p_2(t) &= t^2 + r_2 t + q_2,
\end{align*}

$p_1$ and $p_2$ can be examined separately.
In order to find the solutions $t_1$ and $t_3$ (i.e., the times where
the trajectory is inward directed), we impose the condition
$p'(t) \leq 0$ ($p(t)$ is the squared distance of the trajectory to the
circle).

At a zero $t_*$ of $p_1(t)$, the derivative can be expressed as
$$
p'(t_*) = c p_1'(t_*) p_2(t_*) + c p_1(t_*) p_2'(t_*) = c p_1'(t_*) p_2(t_*).
$$
This can be used to reduce the number of floating point operations in
the algorithm, to determine the first zero $t_* \geq 0$ for which
$p'(t_*) \leq 0$.
  
\section{Elastic collision}
The velocities $\boldsymbol{v}_A'$ and $\boldsymbol{v}_B'$ of the
balls $A$ and $B$ after the collisions are given by:

\begin{subequations}
  \begin{align}
    \label{eq:collision:A}
    \boldsymbol{v}_A' & = \boldsymbol{v}_A - 2 \frac{m_B}{m_A + m_B} \frac{\langle
                 \boldsymbol{\Delta x}, \boldsymbol{\Delta v}
                 \rangle}{\|\boldsymbol{\Delta x}\|^2}
                 \boldsymbol{\Delta x} \\
    \label{eq:collision:B}
    \boldsymbol{v}_B' & = \boldsymbol{v}_B + 2 \frac{m_A}{m_A + m_B} \frac{\langle
                 \boldsymbol{\Delta x}, \boldsymbol{\Delta v}
                 \rangle}{\|\boldsymbol{\Delta x}\|^2}
                 \boldsymbol{\Delta x}
  \end{align}
\end{subequations}

\subsection{Derivation}

In the center of mass system $S$, of the balls, which has relative
velocity $\boldsymbol{v}_S = \frac{\boldsymbol{p}_A +
  \boldsymbol{p}_B}{m_A+m_B}$ to the original system, we have:

\begin{equation}
  \label{eq:momentum:S}
  \boldsymbol{p}_A^{(S)} + \boldsymbol{p}_B^{(S)} = 0
  \quad \Leftrightarrow \quad
  \boldsymbol{p}_A^{(S)} = -\boldsymbol{p}_B^{(S)},
\end{equation}
where
$\boldsymbol{p}_A^{(S)} := m_A (\boldsymbol{v}_A-\boldsymbol{v}_S)$
and
$\boldsymbol{p}_S^{(S)} := m_B (\boldsymbol{v}_B-\boldsymbol{v}_S)$
designate the momenta of the balls in the center of mass system.

In both systems momentum and energy is conserved:

\begin{align}
  \label{eq:momentum}\tag{P}
  \boldsymbol{p}_A + \boldsymbol{p}_B
  & = \boldsymbol{p}_A' + \boldsymbol{p}_B'
  & \textrm{Momentum}\\
  \label{eq:angular_momentum}\tag{L}
  \boldsymbol{x}_A \times \boldsymbol{p}_A + \boldsymbol{x}_B \times \boldsymbol{p}_B
  & = \boldsymbol{x}_A \times \boldsymbol{p}_A' + \boldsymbol{x}_B \times \boldsymbol{p}_B'
  & \textrm{Angular Momentum} \\
  \label{eq:energy}\tag{E}
  \tfrac{m_A}{2} \|\boldsymbol{v}_A \|^2
  + \tfrac{m_B}{2} \|\boldsymbol{v}_B \|^2
  & = \tfrac{m_A}{2} \|\boldsymbol{v}_A' \|^2
    + \tfrac{m_B}{2} \|\boldsymbol{v}_B' \|^2 
  & \textrm{Energy}
\end{align}
using \eqref{eq:momentum}, we obtain
${\boldsymbol{p}_A^{(S)}}' = -{\boldsymbol{p}_B^{(S)}}'$, and thus with
\eqref{eq:energy}:
\begin{equation}
  \label{eq:abs}
  \| \boldsymbol{v}_A^{(S)} \| = \| {\boldsymbol{v}_A^{(S)}}' \|,
  \qquad
  \| \boldsymbol{v}_B^{(S)} \| = \| {\boldsymbol{v}_B^{(S)}}' \|.
\end{equation}

Inserting \eqref{eq:momentum:S} into \eqref{eq:angular_momentum} yields:
\begin{align*}
  &&\boldsymbol{x}_A \times \boldsymbol{p}_A^{(S)} - \boldsymbol{x}_B
  \times \boldsymbol{p}_A^{(S)}
  & = \boldsymbol{x}_A \times {\boldsymbol{p}_A^{(S)}}' -
    \boldsymbol{x}_B \times {\boldsymbol{p}_A^{(S)}}'\\
  &\Leftrightarrow&
  (\boldsymbol{x}_A - \boldsymbol{x}_B) \times \boldsymbol{p}_A^{(S)}
  & = (\boldsymbol{x}_A - \boldsymbol{x}_B) \times
    {\boldsymbol{p}_A^{(S)}}'\\
  &\Leftrightarrow&
  \boldsymbol{\Delta x} \times (\boldsymbol{p}_A^{(S)} - {\boldsymbol{p}_A^{(S)}}') & = 0.
\end{align*}
Because $\boldsymbol{\Delta x} \neq 0$, this only can occur, it either
$\boldsymbol{p}_A^{(S)} = {\boldsymbol{p}_A^{(S)}}'$ (no collision,
balls overlap) or
$\boldsymbol{p}_A^{(S)} - {\boldsymbol{p}_A^{(S)}}'$ is parallel to
$\boldsymbol{\Delta x}$.

The latter combined with \eqref{eq:abs} means, that
${\boldsymbol{p}_A^{(S)}}'$ is the reflection of
$\boldsymbol{p}_A^{(S)}$ on the perpendicular of $\boldsymbol{\Delta
  x}$ through the center of ball $A$ (see
Figure~\ref{fig:collision:S}).
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{collision_S}
  \caption{Collision of two balls in the center of mass system}
  \label{fig:collision:S}
\end{figure}
\begin{align*}
{\boldsymbol{v}_A^{(S)}}'
  & = \boldsymbol{v}_A^{(S)} - 2 \langle \tfrac{\boldsymbol{\Delta x}}{\|\boldsymbol{\Delta x}\|}, \boldsymbol{v}_A^{(S)} \rangle \tfrac{\boldsymbol{\Delta x}}{\|\boldsymbol{\Delta x}\|}\\
  \qquad \Leftrightarrow
  \boldsymbol{v}_A'
  & = \boldsymbol{v}_A  - 2 \langle \tfrac{\boldsymbol{\Delta x}}{\|\boldsymbol{\Delta x}\|}, \boldsymbol{v}_A^{(S)} \rangle \tfrac{\boldsymbol{\Delta x}}{\|\boldsymbol{\Delta x}\|}.
\end{align*}
Inserting
$\boldsymbol{v}_A^{(S)} = \boldsymbol{v}_A - \boldsymbol{v}_S =
\frac{m_B}{m_A + m_B} \boldsymbol{\Delta v}$, immediately leads to
\eqref{eq:collision:A} and \eqref{eq:collision:B}.
\end{document}
