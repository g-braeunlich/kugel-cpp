# Kugel

A small physics simulation of balls / rectangles confined within
boundary walls, optionally interacting via a central force.
The central force can behave like gravity or the electric force
according to the sign of the force strength constant. A Lorentz force
is currently not implemented.

![kugel](doc/kugel.svg)

**Attention**: The numerical integration of central forces becomes
inaccurate if the time step is to large (in this simulation the
time step is fixed, but the strength of the central force can be
modified). If the forces become too strong, expect unphysical
behaviour.


## Gallery

![gallery](doc/gallery.svg)

## Install

The latest binary build for windows is available
[here](../-/jobs/artifacts/master/raw/build-mingw/src/app/kugel.exe?job=exe)

Source code bundles
[here](../-/jobs/artifacts/master/raw/build/meson-dist/kugel-latest.tar.xz?job=dist).

On linux the only runtime dependencies are `X11` and `cairo` (already
included in most desktop distributions).

```bash
sudo apt-get install X11 libcairo2
```

The windows binary should be statically linked and should not require
any dependencies.

## Usage

Just run the binary `kugel`. Either with a single argument (a `json`
file, see [examples](examples)) or without. During the simulation, the following key
combinations are available (in the windows version only `Q`, `Esc`,
`Space` are working):

| Key          | Action                                       |
|--------------|----------------------------------------------|
| `Q` or `Esc` | Quit                                         |
| `Space`      | Pause                                        |
| `P`          | Take a svg screenshot                        |
| `R`          | Start / stop recording motion trace to a svg |
| `↑`          | Increase frame rate                          |
| `↓`          | Decrease frame rate                          |

The screenshots / motion traces are stored in files `/tmp/kugel-YYYY-mm-ddTHHMMSS.svg`.

## Build from source

```bash
meson <build_dir>
```

```bash
meson compile -C <build_dir>
```

### Build Dependencies

- [meson](https://mesonbuild.com/) (build system)
- [nlohmann_json](https://github.com/nlohmann/json)
- [cairo](https://www.cairographics.org/)
- [x11](https://github.com/mirror/libX11) (linux only)
- [gtest](https://github.com/google/googletest) (optional)


### Run unit tests

```bash
meson test -C <build_dir>
```

## Mathematical model

For a detailed description, see the 
[separate documentation (pdf)](../-/jobs/artifacts/master/raw/build-doc/physical_model.pdf?job=doc)


## Credits

This project originated around 2001, when our math teacher, Peter
Strebel introduced us to java. A year later, I continued work on the inital idea in my Matura
Thesis under his supervision.
