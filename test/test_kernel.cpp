#include <gtest/gtest.h>
#include <unordered_set>
#include <utility>
#include "kernel.cpp"

TEST(KernelTest, Kernel)
{
  Kernel core(Polygon{});
  Ball B1, B2;
  B1.r = 10.;
  B2.r = 10.;
  B2.x = 20.;

  core.add_movable(&B1);
  core.add_movable(&B2);
}

TEST(DefaultConfigTest, Kernel) {
  Kernel core;
  Kernel::default_configuration(core, 100., 100., 5);
}
