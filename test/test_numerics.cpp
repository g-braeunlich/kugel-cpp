#include <gtest/gtest.h>
#include "numerics.cpp"

const double eps = 1.E-10;

TEST(NumericsTest, zero_p4_1) {
  // p(x) = (x + 1)(x - 1)(x - 2)(x - 3) = (x^2-1)(x^2-5x+6)
  //      = x^4 - 5x^3 + 5x^2 + 5x - 6
  double p[] = {-6., 5., 5., -5., 1.};
  double t = zero_p4(p, eps);
  ASSERT_LE(fabs(t-2), eps/2);
}

TEST(NumericsTest, zero_p4_2) {
  // p(x) = (x + 1)(x - 1)^2(x - 2) = (x^2-1)(x^2-3x+2)
  //      = x^4 - 3x^3 + x^2 + 3x - 2
  double p[] = {-2., 3., 1., -3., 1.};
  double t = zero_p4(p, eps);
  ASSERT_LE(fabs(t-1), eps/2);
}

TEST(NumericsTest, zero_p4_3) {
  // p(x) = (x^2 + 1)(x - 1)(x - 2) = (x^2+1)(x^2-3x+2)
  //      = x^4 - 3x^3 + 3x^2 - 3x + 2
  double p[] = {2., -3., 3., -3., 1.};
  double t = zero_p4(p, eps);
  ASSERT_LE(fabs(t-1), eps/2);
}

TEST(NumericsTest, zero_p4_4) {
  // p(x) = (x^2 + 1)(x^2 + 2)
  //      = x^4 + 3x^2 + 2
  double p[] = {2., 0., 3., 0., 1.};
  double t = zero_p4(p, eps);
  ASSERT_LE(fabs(t-undefined), eps/2);
}
