#include <gtest/gtest.h>
#include <unordered_set>
#include <utility>

#include "multi_dispatch.hpp"

struct DispatchTable_2
{
  static void dispatch(int& result, Ball&, Ball&) { result = 1; }
  static void dispatch(int& result, Ball&, Wall&) { result = 2; }
  static void dispatch(int& result, Wall&, Ball&) { result = 2; }
  static void dispatch(int& result, Wall&, Wall&) { result = -1; }
  static void dispatch(int& result, Ball&, Rectangle&) { result = -1; }
  static void dispatch(int& result, Rectangle&, Ball&) { result = -1; }
  static void dispatch(int& result, Rectangle&, Rectangle&) { result = -1; }
  static void dispatch(int& result, Wall&, Rectangle&) { result = -1; }
  static void dispatch(int& result, Rectangle&, Wall&) { result = -1; }
  DispatchTable_2() = default;
};

struct DispatchTable_3
{
  static void dispatch(int &result, Ball&, Ball&, Ball&) { result = 1; }
  static void dispatch(int &result, Ball&, Ball&, Wall&) { result = 2; }
  static void dispatch(int &result, Ball&, Ball&, Rectangle&) { result = 3; }
  static void dispatch(int &result, Ball&, Wall&, Ball&) { result = 4; }
  static void dispatch(int &result, Ball&, Wall&, Wall&) { result = 5; }
  static void dispatch(int &result, Ball&, Wall&, Rectangle&) { result = 6; }
  static void dispatch(int &result, Ball&, Rectangle&, Ball&) { result = 7; }
  static void dispatch(int &result, Ball&, Rectangle&, Wall&) { result = 8; }
  static void dispatch(int &result, Ball&, Rectangle&, Rectangle&) { result = 9; }
  
  static void dispatch(int &result, Wall&, Ball&, Ball&) { result = 11; }
  static void dispatch(int &result, Wall&, Ball&, Wall&) { result = 12; }
  static void dispatch(int &result, Wall&, Ball&, Rectangle&) { result = 13; }
  static void dispatch(int &result, Wall&, Wall&, Ball&) { result = 14; }
  static void dispatch(int &result, Wall&, Wall&, Wall&) { result = 15; }
  static void dispatch(int &result, Wall&, Wall&, Rectangle&) { result = 16; }
  static void dispatch(int &result, Wall&, Rectangle&, Ball&) { result = 17; }
  static void dispatch(int &result, Wall&, Rectangle&, Wall&) { result = 18; }
  static void dispatch(int &result, Wall&, Rectangle&, Rectangle&) { result = 19; }
  
  static void dispatch(int &result, Rectangle&, Ball&, Ball&) { result = 21; }
  static void dispatch(int &result, Rectangle&, Ball&, Wall&) { result = 22; }
  static void dispatch(int &result, Rectangle&, Ball&, Rectangle&) { result = 23; }
  static void dispatch(int &result, Rectangle&, Wall&, Ball&) { result = 24; }
  static void dispatch(int &result, Rectangle&, Wall&, Wall&) { result = 25; }
  static void dispatch(int &result, Rectangle&, Wall&, Rectangle&) { result = 26; }
  static void dispatch(int &result, Rectangle&, Rectangle&, Ball&) { result = 27; }
  static void dispatch(int &result, Rectangle&, Rectangle&, Wall&) { result = 28; }
  static void dispatch(int &result, Rectangle&, Rectangle&, Rectangle&) { result = 29; }

  DispatchTable_3() = default;
};



TEST(TupleDispatcherTest, Dispatch)
{
  Ball B;
  Wall W(0., 0., 0., 1.);
  Rectangle R;
  int result = 0;

  dispatch<DispatchTable_2, void, int&>(result, (Object&)B, (Object&)W);
  EXPECT_EQ(result, 2);
  dispatch<DispatchTable_2, void, int&>(result, (Object&)B, (Object&)B);
  EXPECT_EQ(result, 1);
  dispatch<DispatchTable_2, void, int&>(result, (Object&)B, (Object&)R);
  EXPECT_EQ(result, -1);

  dispatch<DispatchTable_3, void, int&>(result, (Object&)B, (Object&)B, (Object&)B);
  EXPECT_EQ(result, 1);
  dispatch<DispatchTable_3, void, int&>(result, (Object&)B, (Object&)B, (Object&)W);
  EXPECT_EQ(result, 2);
  dispatch<DispatchTable_3, void, int&>(result, (Object&)B, (Object&)R, (Object&)W);
  EXPECT_EQ(result, 8);
}
