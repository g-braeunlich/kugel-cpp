#include <gtest/gtest.h>
#include <unordered_set>
#include <utility>
#include "relation_manager.hpp"

TEST(RelationManagerTest, Relations)
{
  RelationManager<int> rm;
  std::unordered_set<int> relation_ids = {1, 2, 3, 4};
  std::unordered_set<int> property_ids = {-1, -2, -3, -4};
  rm.add_object(1);
  rm.add_object(2);
  rm.add_object(3);
  rm.add_object(4);
  rm.add_relation(1, 2, [&rels=relation_ids]() {rels.erase(1);});
  rm.add_relation(1, 2, [&rels=relation_ids]() {rels.erase(2);});
  rm.add_relation(3, 1, [&rels=relation_ids]() {rels.erase(3);});
  rm.add_relation(3, 4, [&rels=relation_ids]() {rels.erase(4);});

  std::unordered_set<int> reference_set = {1, 2, 3, 4};
  EXPECT_EQ(relation_ids, reference_set);

  rm.remove_object(3);
  reference_set = {1, 2};
  EXPECT_EQ(relation_ids, reference_set);

  rm.remove_object(2);
  EXPECT_TRUE(relation_ids.empty());

  rm.add_remover(1, [&props=property_ids]() {props.erase(-1);});
  rm.remove_object(1);
  reference_set = {-2, -3, -4};
  EXPECT_EQ(property_ids, reference_set);
}
